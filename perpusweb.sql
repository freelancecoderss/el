-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 19 Agu 2017 pada 02.57
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpusweb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(20) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `username`, `password`, `foto`) VALUES
(1, 'Admin', 'admin', 'admin', 'assets/images/admin/avatar5.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` varchar(8) NOT NULL,
  `no_induk` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `foto` varchar(225) NOT NULL,
  `tgl_mendaftar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `no_induk`, `nama`, `username`, `password`, `foto`, `tgl_mendaftar`) VALUES
('20170001', '1236758912', 'Anggota 1', 'anggota1', 'anggota1', 'assets/images/anggota/user.png', '2017-08-19 00:54:07'),
('20170002', '0912381287', 'Anggota 2', 'anggota2', 'anggota2', 'assets/images/anggota/user.png', '2017-08-19 00:54:07'),
('20170003', '9281736152', 'Anggota 3', 'anggota3', 'anggota3', 'assets/images/anggota/user.png', '2017-08-19 00:54:08'),
('20170004', '0927465328', 'Anggota 4', 'anggota4', 'anggota4', 'assets/images/anggota/user.png', '2017-08-19 00:54:08'),
('20170005', '8273650192', 'Anggota 5', 'anggota5', 'anggota5', 'assets/images/anggota/user.png', '2017-08-19 00:54:08'),
('20170006', '8471620385', 'Anggota 6', 'anggota6', 'anggota6', 'assets/images/anggota/user.png', '2017-08-19 00:54:08'),
('20170007', '9483716254', 'Anggota 7', 'anggota7', 'anggota7', 'assets/images/anggota/user.png', '2017-08-19 00:54:08'),
('20170008', '7261530478', 'Anggota 8', 'anggota8', 'anggota8', 'assets/images/anggota/user.png', '2017-08-19 00:54:08'),
('20170009', '8372615479', 'Anggota 9', 'anggota9', 'anggota9', 'assets/images/anggota/user.png', '2017-08-19 00:54:08'),
('20170010', '9281736452', 'Anggota 10', 'anggota10', 'anggota10', 'assets/images/anggota/user.png', '2017-08-19 00:54:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(11) NOT NULL,
  `judul_buku` varchar(100) NOT NULL,
  `pengarang` varchar(150) NOT NULL,
  `penerbit` varchar(150) NOT NULL,
  `tahun_terbit` varchar(4) NOT NULL,
  `isbn` varchar(20) NOT NULL,
  `tgl_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`id_buku`, `judul_buku`, `pengarang`, `penerbit`, `tahun_terbit`, `isbn`, `tgl_input`) VALUES
(1, 'Matematika untuk Kelas VII', 'Budi Santoso', 'Erlangga', '2015', '602-220-150-0', '2017-07-28 12:21:33'),
(2, 'B. Indonesia untuk Kelas VII', 'Deni Ramdani', 'Erlangga', '2016', '602-220-150-3', '2017-07-28 12:21:33'),
(3, 'IPS untuk Kelas VII', 'Tono Martono', 'Pujangga Muda', '2014', '602-220-150-2', '2017-07-28 12:20:44'),
(4, 'IPA untuk Kelas VII', 'Robi Sujana', 'Pustaka Raya', '2015', '602-220-150-9', '2017-07-28 12:20:44'),
(5, 'Matematika untuk Kelas VIII', 'Rendro Sartono', 'Erlangga', '2016', '602-220-150-4', '2017-07-28 12:20:44'),
(6, 'B. Indonesia untuk Kelas VIII', 'Roni Sucipto', 'Pujangga Muda', '2015', '602-220-150-7', '2017-07-28 12:20:44'),
(7, 'IPA Untuk Kelas VIII', 'Ranti Suryani', 'Pustaka Raya', '2015', '602-220-148-3', '2017-07-28 12:20:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pengembalian`
--

CREATE TABLE `detail_pengembalian` (
  `id_pengembalian` varchar(12) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `tanggal_dikembalikan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pengembalian`
--

INSERT INTO `detail_pengembalian` (`id_pengembalian`, `id_buku`, `tanggal_dikembalikan`) VALUES
('KMB0617001', 2, '2017-06-13'),
('KMB0617001', 3, '2017-06-19'),
('KMB0617002', 4, '2017-06-26'),
('KMB0617003', 1, '2017-06-26'),
('KMB0617004', 6, '2017-06-26'),
('KMB0617004', 7, '2017-06-28'),
('KMB0617005', 4, '2017-06-27'),
('KMB0717001', 3, '2017-07-15'),
('KMB0717002', 2, '2017-07-17'),
('KMB0717002', 1, '2017-07-17'),
('KMB0717003', 1, '2017-07-19'),
('KMB0817001', 4, '2017-08-03'),
('KMB0817001', 6, '2017-08-03'),
('KMB0817002', 1, '2017-08-07'),
('KMB0817002', 4, '2017-08-07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_pinjam` varchar(12) NOT NULL,
  `id_buku` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_pinjam`, `id_buku`) VALUES
('PJM0617001', 2),
('PJM0617001', 3),
('PJM0617002', 4),
('PJM0617003', 1),
('PJM0617004', 6),
('PJM0617004', 7),
('PJM0617005', 4),
('PJM0717001', 3),
('PJM0717002', 2),
('PJM0717002', 1),
('PJM0717003', 1),
('PJM0717004', 4),
('PJM0717004', 6),
('PJM0717005', 1),
('PJM0717005', 2),
('PJM0717006', 4),
('PJM0717006', 1),
('PMJ0817001', 1),
('PMJ0817001', 1),
('PMJ0817002', 3),
('PMJ0817002', 4),
('PMJ0817003', 1),
('PMJ0817003', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ebook`
--

CREATE TABLE `ebook` (
  `id_ebook` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `pengarang` varchar(100) NOT NULL,
  `nama_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ebook`
--

INSERT INTO `ebook` (`id_ebook`, `judul`, `pengarang`, `nama_file`) VALUES
(1, 'Konsep Constraint pada Database', 'Deni Rukanda', 'Constraint on Database Concept.pdf'),
(2, 'Belajar SQL', 'Eli Sulistiawati', 'Data Definition Language.pdf'),
(3, 'Integritas Data Pada Pemrograman Database', 'Ramdan Amirudin', 'Data Integrity.pdf'),
(4, 'Belajar Fungsi pada Pemrograman', 'Muhammad Fikri', 'General Conversion Function.pdf'),
(5, 'Konsep Pembangunan Database', 'Reinaldi Hidayat', 'Join.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `header_pengembalian`
--

CREATE TABLE `header_pengembalian` (
  `id_pengembalian` varchar(12) NOT NULL,
  `id_pinjam` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `header_pengembalian`
--

INSERT INTO `header_pengembalian` (`id_pengembalian`, `id_pinjam`) VALUES
('KMB0617001', 'PJM0617001'),
('KMB0617002', 'PJM0617002'),
('KMB0617003', 'PJM0617003'),
('KMB0617004', 'PJM0617004'),
('KMB0617005', 'PJM0617005'),
('KMB0717001', 'PJM0717001'),
('KMB0717002', 'PJM0717002'),
('KMB0717003', 'PJM0717003'),
('KMB0817001', 'PJM0717004'),
('KMB0817002', 'PMJ0817003');

-- --------------------------------------------------------

--
-- Struktur dari tabel `header_pinjam`
--

CREATE TABLE `header_pinjam` (
  `id_pinjam` varchar(12) NOT NULL,
  `id_anggota` varchar(8) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `header_pinjam`
--

INSERT INTO `header_pinjam` (`id_pinjam`, `id_anggota`, `tanggal_pinjam`, `tanggal_kembali`) VALUES
('PJM0617001', '20170001', '2017-06-06', '2017-06-13'),
('PJM0617002', '20170008', '2017-06-11', '2017-06-18'),
('PJM0617003', '20170004', '2017-06-19', '2017-06-26'),
('PJM0617004', '20170006', '2017-06-19', '2017-06-26'),
('PJM0617005', '20170009', '2017-06-04', '2017-06-27'),
('PJM0717001', '20170007', '2017-07-07', '2017-07-14'),
('PJM0717002', '20170006', '2017-07-10', '2017-07-17'),
('PJM0717003', '20170006', '2017-07-12', '2017-07-19'),
('PJM0717004', '20170004', '2017-07-22', '2017-07-29'),
('PJM0717005', '20170005', '2017-07-24', '2017-07-31'),
('PJM0717006', '20170007', '2017-07-29', '2017-07-31'),
('PMJ0817001', '20170009', '2017-08-03', '2017-08-10'),
('PMJ0817002', '20170008', '2017-08-03', '2017-08-03'),
('PMJ0817003', '20170008', '2017-08-07', '2017-08-10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengunjung`
--

CREATE TABLE `pengunjung` (
  `id_pengunjung` varchar(10) NOT NULL,
  `no_induk` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `keperluan` varchar(250) NOT NULL,
  `waktu_berkunjung` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengunjung`
--

INSERT INTO `pengunjung` (`id_pengunjung`, `no_induk`, `nama`, `keperluan`, `waktu_berkunjung`) VALUES
('PNJ0617001', '0192837654', 'Andri Haryadi', 'Membaca Buku', '2017-06-04 17:00:00'),
('PNJ0617002', '9281746539', 'Dinda Audia', 'Membaca Buku', '2017-06-04 17:00:00'),
('PNJ0617003', '8271645309', 'Rini Widiawati', 'Mengerjakan Tugas', '2017-06-04 17:00:00'),
('PNJ0617004', '9716254789', 'Diki Hariadi', 'Mengerjakan Tugas', '2017-06-04 17:00:00'),
('PNJ0617005', '9281726354', 'Reki Rahman', 'Membaca Buku', '2017-06-04 17:00:00'),
('PNJ0617006', '0192837654', 'Andri Haryadi', 'Membaca Buku', '2017-06-12 17:00:00'),
('PNJ0617007', '8271645309', 'Rini Widiawati', 'Mengerjakan Tugas', '2017-06-12 17:00:00'),
('PNJ0617008', '2881726457', 'Agung Daryono', 'Membaca Buku', '2017-06-12 17:00:00'),
('PNJ0617009', '9281746539', 'Dinda Audia', 'Mengerjakan Tugas', '2017-06-12 17:00:00'),
('PNJ0617010', '7261547843', 'Rahmat Haryadi', 'Mengerjakan Tugas', '2017-06-12 17:00:00'),
('PNJ0617011', '8271645309', 'Rini Widiawati', 'Membaca Buku', '2017-06-19 17:00:00'),
('PNJ0617012', '0192837654', 'Andri Haryadi', 'Membaca Buku', '2017-06-19 17:00:00'),
('PNJ0617013', '2881726457', 'Agung Daryono', 'Membaca Buku', '2017-06-19 17:00:00'),
('PNJ0617014', '8271645389', 'Doni Mulyawan', 'Membaca Buku', '2017-06-19 17:00:00'),
('PNJ0617015', '9716254789', 'Diki Hariadi', 'Mengerjakan Tugas', '2017-06-19 17:00:00'),
('PNJ0617016', '8271645309', 'Rini Widiawati', 'Mengerjakan Tugas', '2017-06-28 17:00:00'),
('PNJ0617017', '9281746539', 'Dinda Audia', 'Membaca Buku', '2017-06-28 17:00:00'),
('PNJ0617018', '8172645098', 'Sintia Nurahma', 'Membaca Buku', '2017-06-28 17:00:00'),
('PNJ0617019', '2881726457', 'Agung Daryono', 'Mengerjakan Tugas', '2017-06-28 17:00:00'),
('PNJ0617020', '9281726354', 'Reki Rahman', 'Membaca Buku', '2017-06-28 17:00:00'),
('PNJ0717001', '0192837654', 'Andri Haryadi', 'Membaca Buku', '2017-07-06 17:00:00'),
('PNJ0717002', '9281746539', 'Dinda Audia', 'Membaca Buku', '2017-07-06 17:00:00'),
('PNJ0717003', '8271645309', 'Rini Widiawati', 'Mengerjakan Tugas', '2017-07-06 17:00:00'),
('PNJ0717004', '9716254789', 'Diki Hariadi', 'Mengerjakan Tugas', '2017-07-06 17:00:00'),
('PNJ0717005', '9281726354', 'Reki Rahman', 'Membaca Buku', '2017-07-06 17:00:00'),
('PNJ0717006', '0192837654', 'Andri Haryadi', 'Membaca Buku', '2017-07-14 17:00:00'),
('PNJ0717007', '8271645309', 'Rini Widiawati', 'Mengerjakan Tugas', '2017-07-14 17:00:00'),
('PNJ0717008', '2881726457', 'Agung Daryono', 'Membaca Buku', '2017-07-14 17:00:00'),
('PNJ0717009', '9281746539', 'Dinda Audia', 'Mengerjakan Tugas', '2017-07-14 17:00:00'),
('PNJ0717010', '7261547843', 'Rahmat Haryadi', 'Mengerjakan Tugas', '2017-07-14 17:00:00'),
('PNJ0717011', '8271645309', 'Rini Widiawati', 'Membaca Buku', '2017-07-21 17:00:00'),
('PNJ0717012', '0192837654', 'Andri Haryadi', 'Membaca Buku', '2017-07-21 17:00:00'),
('PNJ0717013', '2881726457', 'Agung Daryono', 'Membaca Buku', '2017-07-21 17:00:00'),
('PNJ0717014', '8271645389', 'Doni Mulyawan', 'Membaca Buku', '2017-07-21 17:00:00'),
('PNJ0717015', '9716254789', 'Diki Hariadi', 'Mengerjakan Tugas', '2017-07-21 17:00:00'),
('PNJ0717016', '8271645309', 'Rini Widiawati', 'Mengerjakan Tugas', '2017-07-26 17:00:00'),
('PNJ0717017', '9281746539', 'Dinda Audia', 'Membaca Buku', '2017-07-26 17:00:00'),
('PNJ0717018', '8172645098', 'Sintia Nurahma', 'Membaca Buku', '2017-07-26 17:00:00'),
('PNJ0717019', '2881726457', 'Agung Daryono', 'Mengerjakan Tugas', '2017-07-26 17:00:00'),
('PNJ0717020', '9281726354', 'Reki Rahman', 'Membaca Buku', '2017-07-26 17:00:00'),
('PNJ0817001', '987123798246', 'Arya Mahardika', 'Membaca Buku', '2017-08-04 08:10:54'),
('PNJ0817002', '982371654786189', 'Dina Mariana', 'Mengerjakan Tugas', '2017-08-07 10:19:15'),
('PNJ0817003', '87364510298', 'Tono Surtono', 'Membaca Buku', '2017-08-07 10:32:17'),
('PNJ0817004', '98726345123', 'Tine Martine', 'Mengerjakan Tugas', '2017-08-07 10:49:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `detail_pengembalian`
--
ALTER TABLE `detail_pengembalian`
  ADD KEY `id_pengembalian` (`id_pengembalian`),
  ADD KEY `id_buku` (`id_buku`);

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD KEY `id_pinjam` (`id_pinjam`),
  ADD KEY `id_buku` (`id_buku`);

--
-- Indexes for table `ebook`
--
ALTER TABLE `ebook`
  ADD PRIMARY KEY (`id_ebook`);

--
-- Indexes for table `header_pengembalian`
--
ALTER TABLE `header_pengembalian`
  ADD PRIMARY KEY (`id_pengembalian`),
  ADD KEY `header_pengembalian_ibfk_1` (`id_pinjam`);

--
-- Indexes for table `header_pinjam`
--
ALTER TABLE `header_pinjam`
  ADD PRIMARY KEY (`id_pinjam`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indexes for table `pengunjung`
--
ALTER TABLE `pengunjung`
  ADD PRIMARY KEY (`id_pengunjung`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ebook`
--
ALTER TABLE `ebook`
  MODIFY `id_ebook` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_pengembalian`
--
ALTER TABLE `detail_pengembalian`
  ADD CONSTRAINT `detail_pengembalian_ibfk_1` FOREIGN KEY (`id_pengembalian`) REFERENCES `header_pengembalian` (`id_pengembalian`),
  ADD CONSTRAINT `detail_pengembalian_ibfk_2` FOREIGN KEY (`id_buku`) REFERENCES `buku` (`id_buku`);

--
-- Ketidakleluasaan untuk tabel `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_pinjam`) REFERENCES `header_pinjam` (`id_pinjam`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_pinjam_ibfk_2` FOREIGN KEY (`id_buku`) REFERENCES `buku` (`id_buku`);

--
-- Ketidakleluasaan untuk tabel `header_pengembalian`
--
ALTER TABLE `header_pengembalian`
  ADD CONSTRAINT `header_pengembalian_ibfk_1` FOREIGN KEY (`id_pinjam`) REFERENCES `header_pinjam` (`id_pinjam`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `header_pinjam`
--
ALTER TABLE `header_pinjam`
  ADD CONSTRAINT `header_pinjam_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
