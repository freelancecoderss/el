<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ebook extends CI_Model {

	public function GetData()
	{
		$data = $this->db->select('*')->get('ebook')->result();
		
		return $data;
	}
	
	public function GetWhere($key)
	{
		$data = $this->db->select('*')->where('id_ebook', $key)->get('ebook')->result();
		
		return $data;
	}
	
	public function GetFileEbook($key)
	{
		$data = $this->db->select('nama_file')->where('id_ebook', $key)->get('ebook')->result();
		
		return $data;
	}
}
