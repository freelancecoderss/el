<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_ebook extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		if (!$this->session->has_userdata('session_key')) {
			redirect('c_halamanutama/index');
		}
	}
	public function index()
	{
		$listEbook = $this->ebook->GetData();
		
		$data = array(
			'title'			=>'Data Buku',
			'aktif_db'		=>'w3-teal',
			'listEbook'		=> $listEbook
		);
		
		$this->load->view('layouts/masterHeader', $data);
		$this->load->view('layouts/masterNavbar');
		$this->load->view('baca_ebook/v_list_ebook');
		$this->load->view('layouts/masterFooter');
	}
	public function GetDetail()
	{	
		$key = $_POST['key'];
		$detailEbook = $this->ebook->GetWhere($key);
		
		$data = array(
			'detailEbook' => $detailEbook
		);
		return $this->load->view('baca_ebook/v_detail', $data);
	}
	
	public function BacaEbook($key)
	{
		$fileEbook = $this->ebook->GetFileEbook($key);
		
		$data = array(
			'fileEbook' => $fileEbook
		);
		
		$this->load->view('baca_ebook/v_bacaebook',$data);
	}
}
