<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_anggota extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->has_userdata('session_key')) {
			redirect('c_halamanutama/index');
		}
	}
	public function index()
	{
		$data = array(
			'title'			=>'Beranda',
			'aktif_b'		=>'w3-teal',
		);
		
		$this->load->view('layouts/masterHeader', $data);
		$this->load->view('layouts/masterNavbar');
		$this->load->view('beranda/v_beranda');
		$this->load->view('layouts/masterFooter');
	}
	
	public function logout()
	{
		$this->session->unset_userdata('session_key','useraktif', '__ci_last_regenerate');
		redirect('c_halamanutama/index');
	}
}
