<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_halamanutama extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->has_userdata('session_key')) {
			redirect('c_anggota/index');
		}
	}
	public function index()
	{
		$this->load->view('v_halamanlogin');
	}
	
	public function CekLogin()
	{
		$user = $this->input->post('user');
		$pwd = $this->input->post('pwd');

		$hasil = $this->anggota->VerfLogin($user,$pwd);

		if ($hasil) {
			$user_data = array(
				'session_key' => md5(time()),
				'useraktif' => $hasil[0]->nama,
				'gambar' => $hasil[0]->foto
			);
			$this->session->set_userdata($user_data);
			redirect('c_anggota/index');
		}else {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Maaf Username / Password Anda Salah!</div>');
			redirect('c_halamanutama/index');
		}
	}
}
