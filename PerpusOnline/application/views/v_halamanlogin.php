<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Perpustakaan Online | Halaman Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
  <div class="warapper" style="padding-top:50px;">
    <h2 align="center"><i class="fa fa-book"></i>&nbsp;Perpustakaan Online</h2><hr>
        <div id="page-wrapper" style="width:40%;margin:auto;border-radius:20px;background-color:#00cccc;">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                <div><?php echo $this->session->flashdata('pesan'); ?></div>
                    <div class="col-lg-12" align="center">
                      <form action="<?php echo base_url('c_halamanutama/CekLogin');?>" method="POST" style="width:60%;">
                        <div class="form-group">
                          <label for="">Username</label>
                          <input type="text" class="form-control" id="" placeholder="Username" name="user">
                        </div>
                        <div class="form-group">
                          <label for="">Password</label>
                          <input type="password" class="form-control" id="" placeholder="Password" name="pwd">
                        </div><hr>
                        <div class="btn-group" style="">
                            <button type="submit" class="btn btn-primary">Login <span class="fa fa-sign-in"></span></button>
                            <a href="<?php echo base_url('c_halamanutama'); ?>" class="btn btn-warning"><span class="fa fa-remove"></span> Batal</a>
                        </div>
                      </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</body>
</html>
