<div id="konten">
	 <div class="jumbotron w3-white">
      <div class="container">
        <h1 class="display-3 w3-center"><small>Selamat Datang di </small>Perpustakaan Online <span class="fa fa-book"></span></h1>
        <!-- <p>Anda sedang berada di halaman utama perpustakaan online</p> -->
        <!-- <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p> -->
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4" style="background-color: #fff;">
          <h2>Beranda</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-primary" href="<?php echo base_url(); ?>" role="button">Lihat selengkapnya &raquo;</a></p>
        </div>
        <div class="col-md-4" style="background-color: #fff;">
          <h2>Data Buku</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-primary" href="<?php echo base_url('c_ebook/index'); ?>" role="button">Lihat selengkapnya &raquo;</a></p>
        </div>
        <div class="col-md-4" style="background-color: #fff;">
          <h2>Tentang</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-primary" href="<?php echo base_url(); ?>" role="button">Lihat selengkapnya &raquo;</a></p>
        </div>
      </div>

      <hr>
</div>