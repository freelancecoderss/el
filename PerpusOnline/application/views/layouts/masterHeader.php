<?php date_default_timezone_set('Asia/Jakarta') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>E-Library | <?php if(isset($title)){echo $title;} ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/w3.css');?>" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?php echo base_url('assets/js/DigitalDateTime.js');?>"></script>
    <style media="screen">
      #master-nav{
        opacity:0.9; border-bottom: 4px solid teal;
      }
      #master-nav:hover{
        border-bottom: 4px solid gray;
      }
      #nav-brand:hover{
        border-right:2px solid gray;
      }
    </style>
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
  <div class="warapper" style="padding-top:50px;">