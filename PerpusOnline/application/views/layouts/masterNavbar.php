    <!-- Navigation -->
    <nav class="navbar navbar-fixed-top w3-blue-grey w3-hover-teal" style="" id="master-nav">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar w3-black"></span>
            <span class="icon-bar w3-black"></span>
            <span class="icon-bar w3-black"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>" style="color:white;border-right:2px solid white;" id="nav-brand"><i class="fa fa-book"></i> &nbsp; Perpustakaan Online</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li role="presentation" class="<?php if(isset($aktif_b)){ echo $aktif_b;}?>" style=""><a href="<?php echo base_url();?>">Beranda</a></li>
            <li role="presentation" class="<?php if(isset($aktif_db)){ echo $aktif_db;}?>" style=""><a href="<?php echo base_url('c_ebook/index');?>">Data Buku</a></li>
            <li role="presentation" class="<?php if(isset($aktif_t)){ echo $aktif_t;}?>" style=""><a href="<?php echo base_url();?>">Tentang</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right" style="padding-right: 20px;">
              <li class="" style=""><a href="<?php echo base_url('c_anggota/logout') ?>">Keluar <span class="fa fa-sign-out"></span></a></li>
          </ul>
        </div>
    </nav>
    <div id="page-wrapper" style="min-height:650px;background-color: transparent;">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">