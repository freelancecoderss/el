<?php date_default_timezone_set('Asia/Jakarta') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>E-Library | Landing Page</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/w3.css');?>" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?php echo base_url('assets/js/DigitalDateTime.js');?>"></script>
    <style media="screen">
      #master-nav{
        opacity:0.9; border-bottom: 4px solid teal;
      }
      #master-nav:hover{
        border-bottom: 4px solid gray;
      }
      #nav-brand:hover{
        border-right:2px solid gray;
      }
    </style>
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
  <div class="warapper" style="padding-top:50px;">
        <!-- Navigation -->
    <nav class="navbar navbar-fixed-top w3-blue-grey w3-hover-teal" style="" id="master-nav">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar w3-black"></span>
            <span class="icon-bar w3-black"></span>
            <span class="icon-bar w3-black"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>" style="color:white;border-right:2px solid white;" id="nav-brand"><i class="fa fa-book"></i> &nbsp; E-Library</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li role="presentation" class="" style=""><a href="?">Beranda</a></li>
            <li role="presentation" class="" style=""><a href="">Data Buku</a></li>
            <li role="presentation" class="" style=""><a href="">Kontak</a></li>
            <li role="presentation" class="" style=""><a href="">Tentang</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right" style="padding-right: 20px;">
              <li class="" style=""><a href="../../lib/logout_su.php">Keluar <span class="fa fa-sign-out"></span></a></li>
          </ul>
        </div>
    </nav>
        <div id="page-wrapper" style="min-height:650px;background-color: transparent;">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</body>
</html>
