<header class="panel-heading w3-teal">
	<b>Daftar E-Book</b>
</header>
<div class="panel-body w3-white">
	<table class="table striped">
		<tr>
			<th>No</th>
			<th>Id E-Book</th>
			<th>Judul E-Book</th>
			<th>Detail</th>
		</tr>
		<?php $no = 1; foreach ($listEbook as $dt): ?>
		<tr>
		  <td><?php echo $no++;?></td>
		  <td><?php echo $dt->id_ebook;?></td>
		  <td><span class="fa fa-book"> <?php echo $dt->judul;?></span></td>
		  <td>
		  <center>
		  <div>
			<a class="btn btn-sm btn-primary" class="btn w3-blue" id="idEbook" data-toggle="modal" data-target="#ModalDetail" title="Detail" onclick="fillData('<?=$dt->id_ebook?>')"><span class="glyphicon glyphicon-list"></span></a>

			<div>
			<div class="modal fade" id="ModalDetail" idEbook="<?php echo $dt->id_ebook;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header w3-blue">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Detail Ebook<i style="float:right;"><?php echo $dt->id_ebook;?></i></h4>
			      </div>
				  <center>
				  <div class="modal-body" id="targetDetail">
			      </div>
				  </center>
			    <div class="modal-footer">
					<a class="btn btn-info" href="<?=base_url('c_ebook/BacaEbook/'.$dt->id_ebook);?>" target="_blank"><span class="fa fa-book"></span> Baca E-Book</a>
			        <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="fa fa-remove"></span> Tutup</button>
			     </div>
				</div>
			  </div>
			</div>
		</div>
		  </div>
		  </center>
		  </td>
		</tr>
		<?php endforeach ?> 
	</table>
</div>
<script type="text/javascript">	
function fillData(id){
	var link = "<?php echo base_url('c_ebook/GetDetail/');?>";
	$.ajax({
		type:'POST',
		url:''+link+'',
		data:{key:id},
		success:function(response){
			$('#targetDetail').html(response);
		}
	});
}
</script>