<?php date_default_timezone_set('Asia/Jakarta') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>E-Library | Landing Page</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/w3.css');?>" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<?php echo base_url('assets/js/DigitalDateTime.js');?>"></script>
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
  <div class="warapper" style="padding-top:50px;">
		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:teal;">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
						<!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
						</button> -->
						<a class="navbar-brand" href="<?php echo base_url();?>" style="color:white;border-right:2px solid white;"><i class="fa fa-book"></i> &nbsp; E-Library</a>
				</div>
        <span class="navbar-brand" style="color:white;margin-left:25%;">
          <span class="Tanggal"><script language="JavaScript">document.write(tanggallengkap);</script></span> - <span id="output" class="jam"></span>
        </span>
				<!-- Top Menu Items -->
				<ul class="nav navbar-right top-nav" style="">
						<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Login Admin" style="color:white;"><i class="fa fa-user"></i><b class="caret"></b></a>
								<ul class="dropdown-menu">
										<li><a href="<?php echo base_url('c_halamanutama/loadFormLogin');?>">Login <i class="fa fa-fw fa-sign-in"></i></a ></li>
								</ul>
						</li>
				</ul>
			</nav>
        <div id="page-wrapper" style="background-color:transparent;">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                <div class="alert alert-info alert-dismissable" style="">
                  <b>Selamat Datang di PerPusWeb (Perpustakaan Berbasis Website), Untuk Login Admin silahkan klik Icon User atau klik <a href="<?php echo base_url('c_halamanutama/loadFormLogin');?>"><i>disini</i></a></b>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                </div>
            <div class="col-sm-5" align="">
              <div class="panel panel-default">
                <div class="panel-heading w3-teal">Silahkan Login Untuk Melanjutkan</div>
                <div class="panel-body">
                  <div class="" id="loginPengunjung">
                    <form action="<?=base_url('c_halamanutama/InsertPengunjung');?>" method="POST">
                    <legend><i class="fa fa-user"></i> Login Pengunjung</legend>
                      <div class="form-group">
                        <label>NIS</label>
                        <input type="text" name="nis" required class="form-control">
                      </div>
                      <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" name="nama" required class="form-control">
                      </div>
                      <div class="form-group">
                        <label>Keperluan</label>
                        <input type="text" name="keperluan" required class="form-control">
                      </div>
                      <div class="btn-group">
                        <button class="btn btn-info" type="submit">Masuk <i class="fa fa-sign-in"></i></button>
                        <button class="btn btn-warning" type="button" id="batalP">Batal <i class="fa fa-remove"></i></button>
                      </div>
                    </form>
                  </div>
                  <div class="" style="text-align: center;">
                    <button class="btn btn-info" id="btn_pengunjung">Masuk Sebagai Pengunjung</button> <h3 id="atau">atau</h3>
                    <button class="btn btn-primary" id="btn_anggota">Masuk Sebagai Anggota</button>
                  </div>
                  <div class="" id="loginAnggota">
                  <form action="" method="POST">
                  <legend><i class="fa fa-user"></i> Login Anggota</legend>
                      <div class="form-group">
                        <label>NIS</label>
                        <input type="text" name="nis" required class="form-control">
                      </div>
                      <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" name="nama" required class="form-control">
                      </div>
                      <div class="form-group">
                        <label>Kelas</label>
                        <input type="text" name="kelas" required class="form-control">
                      </div>
                      <div class="btn-group">
                        <button class="btn btn-info" type="submit">Masuk <i class="fa fa-sign-in"></i></button>
                        <button class="btn btn-warning" type="button" id="batalA">Batal <i class="fa fa-remove"></i></button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
                    <div class="col-sm-7" align="">
                      <div class="panel panel-default">
                        <div class="panel-heading w3-teal"><b>DATA PENGUNJUNG HARI INI</b> <i style="float:right;">20 Pengunjung Terakhir</i></div>
                        <div class="panel-body">
                          <table class="table table-hover">
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <!--th>Tanggal</th-->
                              <th>Waktu Kunjungan</th>
                              <th>Keprluan</th>
                            </tr>
                            <?php $no = 1; foreach ($dataHariIni as $dt): ?>
                            <tr>
                              <td><?php echo $no++;?></td>
                              <td><?php echo $dt->nama;?></td>
                              <!--td><?php echo $dt->tgl_kunjung;?></td-->
                              <td><?php echo $dt->waktu_berkunjung;?></td>
                              <td><?php echo $dt->keperluan;?></td>
                            </tr>
                            <?php endforeach ?>
                          </table>
                          <h4><center>Jumlah Pengunjung Hari Ini: <?php echo $jmlHariIni; ?> Orang</center></h4>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-12" align="">
                      <div class="panel panel-default">
                        <div class="panel-heading w3-teal"><b>DATA AKUMULASI PENGUNJUNG E-LIBRARY</b> <i style="float:right;">Ditampilkan 15 Pengunjung Terakhir</i></div>
                        <div class="panel-body">
                          <table class="table table-hover">
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Waktu Kunjungan</th>
                              <th>Keprluan</th>
                            </tr>
                            <?php $num=1; foreach ($akumulasi as $dt): ?>
                            <tr>
                              <td><?php echo $num++;?></td>
                              <td><?php echo $dt->nama;?></td>
                              <td><?php echo $dt->waktu_berkunjung;?></td>
                              <td><?php echo $dt->keperluan;?></td>
                            </tr>
                            <?php endforeach ?>

                          </table>
                          <h4><center>Jumlah Pengunjung Hari Ini: <?php echo $countAkumulasi; ?> Orang</center></h4>
                        </div>
                      </div>
                      <div class="footer col-sm-12 col-md-12" style="background-color:white;padding: 20px;text-align:center;">
                        <div><i>Copyright &copy; E-Library 2017</i></div>
                      </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('#loginPengunjung').hide();
        $('#loginAnggota').hide();
      });
      $('#btn_pengunjung').click(function(){
        $('#loginPengunjung').toggle(500);
        $('#loginAnggota').hide(500);
        $('#btn_pengunjung').hide(500);
        $('#btn_anggota').hide(500);
        $('#atau').hide(500);
      });
      $('#btn_anggota').click(function(){
        $('#loginAnggota').toggle(500);
        $('#loginPengunjung').hide(500);
        $('#btn_pengunjung').hide(500);
        $('#btn_anggota').hide(500);
        $('#atau').hide(500);
      });
      $('#batalA').click(function(){
        $('#loginPengunjung').hide(500);
        $('#loginAnggota').hide(500);
        $('#btn_pengunjung').show(500);
        $('#btn_anggota').show(500);
        $('#atau').show(500);
      });
      $('#batalP').click(function(){
        $('#loginPengunjung').hide(500);
        $('#loginAnggota').hide(500);
        $('#btn_pengunjung').show(500);
        $('#btn_anggota').show(500);
        $('#atau').show(500);
      });
    </script>
</body>
</html>
