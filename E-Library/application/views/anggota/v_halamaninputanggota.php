<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Data Anggota</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
      <div class="warapper" style="padding-top:25px;">
            <div class="container">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12" align="center">
                        <div class="panel panel-default">
                          <div class="panel-heading" style="background-color: teal;color: white;text-align: left;"><b>Form Input Data Anggota Perpustakaan</b></div>
                            <div class="panel-body">
								<div><?php echo $this->session->flashdata('pesan'); ?></div>
                                <form class="form-horizontal" style="" action="<?php echo base_url ('anggota/c_anggota/Aksi_Tambah_Data');?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
                                  <div class="form-group">
                                      <label class="col-md-1 col-sm-1 control-label">ID Anggota</label>
                                      <div class="col-sm-9">
                                          <input name="id" type="text" id="id" class="form-control" value="<?=$id?>" readonly required>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-1 col-sm-1 control-label">No induk</label>
                                      <div class="col-sm-9">
                                          <input name="induk" type="text" id="induk" class="form-control" placeholder="No Induk Siswa" autofocus="on" required>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-1 col-sm-1 control-label">Nama</label>
                                      <div class="col-sm-9">
                                          <input name="nama" type="text" id="nama" class="form-control" placeholder="Nama Anggota" autofocus="on" required>
                                      </div>
                                  </div>
								  <div class="form-group">
									<div class="col-md-1 col-sm-1">
									  <label class=" control-label">Foto</label>
									</div>
									<div class="col-md-1 col-sm-1">
									  <img src="#" id="prevGambar" class=height='300' width='200' style='border: 3px;'>
									</div>
								  </div>
								  <div class="form-group">
									<div class="col-md-5 col-sm-4">
										<input type="file" name="gambar" accept="image/*" onchange="loadFile(event)">
										<p class="help-block">Pilih file foto dari komputer anda.</p>
									</div>
								  </div>
								  <div class="form-group">
                                      <div class="col-sm-11">
                                        <button type="submit" name="simpan" class="btn btn-sm btn-primary"><span class="fa fa-floppy-o"></span> Simpan</button>
                                          <a onclick ="location.reload()" class="btn btn-sm btn-danger">Batal &nbsp;<span class="fa fa-refresh"></span></a>
                                      </div>
                                  </div>
								  <script>
								  var loadFile = function(event) {
									var output = document.getElementById('prevGambar');
									output.src = URL.createObjectURL(event.target.files[0]);
								  };
								  </script>
                                </form>
                                <!-- form -->
                            </div>
                            <!-- panel body -->
                        </div>
                        <!-- panel default -->
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->
            </div>
     </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</body>
</html>
