<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Data Anggota Perpustakaan</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
      <div class="warapper" style="padding-top:25px;">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12" align="center">
                        <div class="panel panel-default">
                          <div class="panel-heading" style="background-color: teal;color: white;text-align: left;"><b>Form Edit Data Anggota</b></div>
                            <div class="panel-body">
                                <form class="form-horizontal" style="" action="<?php echo base_url('anggota/c_anggota/Aksi_Edit_Data');?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">
                                  <div class="form-group">
                                      <div class="col-sm-9">
                                          <input name="id" type="hidden" id="id" class="form-control" placeholder="ID Anggota " autofocus="on" required value="<?php echo $id_anggota;?>">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-1 col-sm-1 control-label">No induk</label>
                                      <div class="col-sm-9">
                                          <input name="induk" type="text" id="induk" class="form-control" placeholder="No Induk Anggota" autofocus="on" required value="<?php echo $no_induk;?>">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-1 col-sm-1 control-label">Nama</label>
                                      <div class="col-sm-9">
                                          <input name="nama" type="text" id="nama" class="form-control" placeholder="Nama Anggota" autofocus="on" required value="<?php echo $nama;?>">
                                      </div>
                                  </div>
								  <div class="form-group">
									<div class="col-md-1 col-sm-1">
									  <label class=" control-label">Foto</label>
									</div>
									<div class="col-md-1 col-sm-1">
									  <img src="<?=base_url($foto)?>" id="prevGambar" class=height='300' width='200' style='border: 3px;'>
									</div>
								  </div>
								  <div class="form-group">
									<div class="col-md-5 col-sm-4">
										<input type="file" name="gambar" accept="image/*" onchange="loadFile(event)">
										<p class="help-block">Pilih file foto dari komputer anda.</p>
									</div>
								  </div>
								  <script>
								  var loadFile = function(event) {
									var output = document.getElementById('prevGambar');
									output.src = URL.createObjectURL(event.target.files[0]);
								  };
								  </script>
                                      <div class="col-sm-11">
                                        <button type="submit" name="ubah" class="btn btn-sm btn-primary"><span class="fa fa-floppy-o"></span> Ubah</button>
											<a href="<?=base_url('anggota/c_anggota/Form_Data_Show')?>" class="btn btn-sm btn-danger">Batal &nbsp;<span class="fa fa-refresh"></span></a>
                                      </div>
                                  </div>
                                </form>
                                <!-- form -->
                            </div>
                            <!-- panel body -->
                        </div>
                        <!-- panel default -->
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->
            </div>
     </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</body>
</html>
