<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>E-Library | Lihat  Data Anggota</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
      <div class="warapper" style="padding-top:25px;">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12" align="center">
                        <div class="panel panel-default">
                          <div class="panel-heading" style="background-color: teal;color: white;text-align: left;"><b>Form Lihat Data Anggota</b></div>
                            <div class="panel-body">
								<div><?php echo $this->session->flashdata('pesan'); ?></div>
                                  <table class="table table-hover table-condensed">
                                    <tr style ="background-color: #ebebe0">
                                      <th style="text-align: center;">No</th>
                                      <th style="text-align: center;">ID Anggota</th>
                                      <th style="text-align: center;">No Induk</th>
                                      <th style="text-align: center;">Nama Anggota</th>
									  <th style="text-align: center;">Foto</th>
                                      <th style="text-align: center;">Aksi</th>
                                    </tr>
                                    <?php $no = 1; foreach ($data as $dt): ?>
                                    <tr>
                                      <td style="text-align: center; vertical-align:middle;"><?php echo $no++;?></td>
                                      <td style="text-align: center; vertical-align:middle"><span class="fa fa-user"> &nbsp; <?php echo $dt->id_anggota;?></span></td>
                                      <td style="text-align: center; vertical-align:middle"><?php echo $dt->no_induk;?></td>
                                      <td style="text-align: center; vertical-align:middle"><?php echo $dt->nama;?></td>
									  <td style="text-align: center; vertical-align:middle"><center><img src="<?=base_url($dt->foto)?>" class="img-circle" height="60" width="55" style="border: 3px;"></center></td>
                                    <td style="text-align: center; vertical-align:middle">
                                      <center>
                                      <div id="thanks">
                                        <a class="btn btn-sm btn-primary" data-placement="bottom" data-toggle="tooltip" title="Edit Anggota" href="<?php echo base_url('anggota/c_anggota/Form_Edit_Show/'.$dt->id_anggota);?>"><span class="glyphicon glyphicon-edit"></span></a>
                                        <a class="btn btn-sm btn-danger tooltips" data-placement="bottom" data-toggle="tooltip" title="Hapus Anggota" href="<?php echo base_url('anggota/c_anggota/Aksi_Hapus_data/'.$dt->id_anggota);?>"><span class="glyphicon glyphicon-trash"></a></center></td></tr></div> 
                                    </tr>
                                    <?php endforeach ?> 
                                  </table>
                                <!-- <h4><center>Jumlah Buku : <?php #echo $jml; ?> Buah</center></h4> -->
                            </div>
                            <!-- panel body -->
                        </div>
                        <!-- panel default -->
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->
            </div>
     </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</body>
</html>
