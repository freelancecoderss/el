<div>
<!-- Main content -->
	<section class="content">
		<div class="row" style="margin-bottom:5px;">
			<div class="col-lg-4 col-md-8">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-users fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge"><?php echo $jmlAnggota;?></div>
								<div>Total Anggota.</div>
							</div>
						</div>
					</div>
					<a href="#">
						<div class="panel-footer">
							<span class="pull-left">Lihat Selengkapnya</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-8">
				<div class="panel panel-green">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-book fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge"><?php echo $jmlBuku;?></div>
								<div>Total Buku.</div>
							</div>
						</div>
					</div>
					<a href="#">
					<div class="panel-footer">
						<span class="pull-left">Lihat Selengkapnya</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
					</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-8">
				<div class="panel panel-yellow">
					<div class="panel-heading">
						<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-shopping-cart fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?php echo $jmlHariIni;?></div>
							<div>Pengunjung Hari Ini.</div>
						</div>
					</div>
				</div>
				<a href="#">
					<div class="panel-footer">
						<span class="pull-left">Lihat Selengkapnya</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
				</div>
			</div>
			<!-- <div class="col-lg-3 col-md-6">
				<div class="panel panel-red">
				<div class="panel-heading">
				<div class="row">
				<div class="col-xs-3">
				<i class="fa fa-support fa-5x"></i>
				</div>
				<div class="col-xs-9 text-right">
				<div class="huge">13</div>
				<div>Support Tickets!</div>
				</div>
				</div>
				</div>
				<a href="#">
				<div class="panel-footer">
				<span class="pull-left">View Details</span>
				<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
				<div class="clearfix"></div>
				</div>
				</a>
				</div>
			</div> -->
		</div>
		<!-- /.row -->
		<!-- Main row -->
		<div class="row">
			<div class="col-md-7 col-sm-7">
				<!--earning graph start-->
				<section class="panel">
					<header class="panel-heading w3-teal">Data Peminjaman Buku yang Harus Dikembalikan.</header>
					<div class="panel-body">
						<table class="display table" id="tabel-kembali">
							<thead>
								<tr>
									<th>No</th>
									<th>Id Pinjam</th>
									<th>Nama Anggota</th>
									<th>Detail</th>
									<th>Pemberitahuan</th>
								</tr>
							</thead>
							<tbody>
							<?php $no=1; foreach($peminjamanHarusKembali as $dt):?>
								<tr>
									<td><?php echo $no++;?></td>
									<td><?php echo $dt->id_pinjam;?></td>
									<td><?php echo $dt->nama;?></td>
									<td><a class="btn w3-blue" id="idPinjam" data-toggle="modal" data-target="#ModalDetail" onclick="fillData('<?php echo $dt->id_pinjam;?>')">Detail</a>
									<td><a href="" class="btn w3-blue">Kirim E-Mail</a></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</section>
				<!--earning graph end-->
			</div>
			<div class="col-lg-5 col-sm-5">
			<!--chat start-->
				<section class="panel">
					<header class="panel-heading w3-teal">Pemberitahuan</header>
					<div class="panel-body" id="noti-box">
						<?php
							$jumlahNotif = 0;
							if(count($anggotaBaru) > 0)
							{
								echo "<div class='alert alert-block alert-danger'>";
								echo "<button data-dismiss='alert' class='close close-sm' type='button'><i class='fa fa-times'></i></button>";								
								echo "<strong>".$anggotaBaru[0]->nama."</strong>, Telah terdaftar menjadi anggota perpustakaan.";
								echo "</div>";
								
								$jumlahNotif = 1;
							}
							if(count($bukuBaru) > 0)
							{
								echo "<div class='alert alert-info'>";
								echo "<button data-dismiss='alert' class='close close-sm' type='button'><i class='fa fa-times'></i></button>";
								echo "<strong>".$bukuBaru[0]->judul_buku."</strong>, Buku bacaan baru yang ada di E-Library.";
								echo "</div>";
								$jumlahNotif = 1;
							}
							if(count($pengunjung) > 0)
							{
								echo "<div class='alert alert-warning'>";
								echo "<button data-dismiss='alert' class='close close-sm' type='button'><i class='fa fa-times'></i></button>";
								echo "<strong>".$pengunjung[0]->nama."</strong> Pengunjung baru di E-Library.";
								echo "</div>";
								$jumlahNotif = 1;
							}
							
							if($jumlahNotif < 1)
							{
								echo "<h3>Tidak ada berita terkini</h3>";
							}
							?>
					</div>
				</section>
			</div>
		</div>
	</section>
</div>
<div>
	<div class="modal fade" id="ModalDetail" idPinjam="<?php echo $dt->id_pinjam;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header w3-blue">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Detail Data Buku Yang Harus Kembali <i style="float:right;"><?php echo $dt->id_pinjam;?></i></h4>
	      </div>
	      <div class="modal-body" id="targetDetail">
	      </div>
	    <div class="modal-footer">
	        <span class="btn-group">
	          <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="fa fa-remove"></span> Tutup</button>
	        </span>
	     </div>
		</div>
	  </div>
	</div>
</div>
<script type="text/javascript">	
function fillData(id){
	var link = "<?php echo base_url('admin/c_dashboard/detailBukuHarusKembali');?>";
	$.ajax({
		type:'POST',
		url:''+link+'',
		data:{key:id},
		success:function(response){
			$('#targetDetail').html(response);
		}
	});
}
</script>