<header class="panel-heading w3-teal">
	<b>Tambah admin</b>
</header>
<div class="panel-body w3-white">
	<div class=""><?php echo $this->session->flashdata('pesan');?></div>
	<form class="form-horizontal" action="<?php echo base_url('admin/c_admin/InsertAdmin');?>" method="POST" enctype="multipart/form-data">
	  <div class="form-group">
		<label class="col-sm-2 control-label"> Nama Lengkap </label>
		<div class="col-sm-4">
			<input type="text" class="form-control" placeholder="Nama Lengkap" name="nama" required>
		</div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-2 control-label">Username</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" placeholder="Username" name="username" required>
		</div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-2 control-label">Password</label>
		<div class="col-sm-4">
			<input type="password" class="form-control" placeholder="Password" name="password" required>
		</div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-2 control-label">Foto</label>
		<div class="col-sm-4">
			<img src="#" id="prevGambar" class=height='300' width='200' style='border: 3px;'>
		</div>
	  </div>
	  <div class="form-group">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-4">
			<input type="file" name="gambar" accept="image/*" onchange="loadFile(event)" required>
			<p class="help-block">Pilih file foto dari komputer anda.</p>
		</div>
	  </div>
	  <script>
	  var loadFile = function(event) {
		var output = document.getElementById('prevGambar');
		output.src = URL.createObjectURL(event.target.files[0]);
	  };
	  </script>
	  <button type="submit" value="simpan" name="simpan" class="btn btn-info"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Submit</button>
	  <a href="<?php echo base_url('admin/c_admin/ShowDataAdmin') ?>" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> &nbsp;Batal </a> 
	</form>	
</div>