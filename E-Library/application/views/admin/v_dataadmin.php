<div class="panel-heading w3-teal">Data Administrator</div>
<div class="panel-body w3-white">
	<div class=""><?php echo $this->session->flashdata('pesan');?></div>
	<table class="table	">
		<tr>
			<th>ID</th>
			<th>Nama Lengkap</th>
			<th>Username</th>
			<th>Password</th>
			<th>Foto</th>
			<th>Opsi</th>
		</tr>
		<?php
		foreach($data as $dt):
			echo "<tr>";
			echo "<td style='vertical-align:middle'>".$dt->id_admin."</td>";
			echo "<td style='vertical-align:middle'>".$dt->nama."</td>";
			echo "<td style='vertical-align:middle'>".$dt->username."</td>";
			echo "<td style='vertical-align:middle'>".$dt->password."</td>";
			echo "<td style='vertical-align:middle'><center><img src=".base_url($dt->foto)." class='img-circle' height='60' width='55' style='border: 3px;'></center></td>";
			echo "<td style='vertical-align:middle'><center>
			<a href='".base_url('admin/c_admin/ShowFormEdit/'.$dt->id_admin.'')."' class='btn btn-warning'><span class='glyphicon glyphicon-edit'></span>Edit</a>
			<a href='".base_url('admin/c_admin/DeleteAdmin/'.$dt->id_admin.'')."' class='btn btn-danger'><span class='glyphicon glyphicon-trash'></span>Hapus</a>
			</center></td>";
			echo "</tr>";
		endforeach;
		?>
	</table>
</div>