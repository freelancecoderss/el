<table class="table">
	<thead>
		<tr>
			<th>No</th>
			<th>Id Anggota</th>
			<th>No Induk</th>
			<th>Nama Anggota</th>
			<th>Banyak Peminjaman</th>
			<th>Terlambat Mengembalikan</th>
			<th>Buku yang Belum Dikembalikan</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach ($data as $dt):?>
		<tr>
			<td><?php echo $no++;?></td>
			<td><?php echo $dt->id_anggota;?></td>
			<td><?php echo $dt->no_induk;?></td>
			<td><?php echo $dt->nama;?></td>
			<td><?php if($dt->jumlah_pinjam != ""){echo $dt->jumlah_pinjam;}else{echo 0;}?></td>
			<td><?php if($dt->terlambat_kembali != ""){echo $dt->terlambat_kembali;}else{echo 0;}?></td>
			<td><?php if($dt->belum_kembali != ""){echo $dt->belum_kembali;}else{echo 0;}?></td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>