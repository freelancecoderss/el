<div class="panel-heading w3-teal">Laporan Pengunjung</div>
<div class="panel-body w3-white">
	<div class="form-group">
		<label>Urutkan Laporan Berdasarkan:</label>
		<select  class="form-control" id="idBulan" onchange="fillData(this.value)">
			<option selected disabled>-- Pilih --</option>
			<option value="" selected="selected">Laporan Keseluruhan</option>
			<?php foreach ($bulan as $dt): ?>
				<option value="<?=$dt->idBulan.'-'.$dt->tahun?>"><?php echo $dt->namaBulan.' - '.$dt->tahun;?></option>
			<?php endforeach ?>
		</select>
	</div>
	<div id="targetDetail" class="form-group">
		<table class="table">
			<thead>
				<tr>
					<th>No</th>
					<th>Id Anggota</th>
					<th>No Induk</th>
					<th>Nama Anggota</th>
					<th>Banyak Peminjaman</th>
					<th>Terlambat Mengembalikan</th>
					<th>Buku yang Belum Dikembalikan</th>
				</tr>
			</thead>
			<tbody>
			<?php $no=1; foreach ($peminjamanKeseluruhan as $dt):?>
				<tr>
					<td><?php echo $no++;?></td>
					<td><?php echo $dt->id_anggota;?></td>
					<td><?php echo $dt->no_induk;?></td>
					<td><?php echo $dt->nama;?></td>
					<td><?php if($dt->jumlah_pinjam != ""){echo $dt->jumlah_pinjam;}else{echo 0;}?></td>
					<td><?php if($dt->terlambat_kembali != ""){echo $dt->terlambat_kembali;}else{echo 0;}?></td>
					<td><?php if($dt->belum_kembali != ""){echo $dt->belum_kembali;}else{echo 0;}?></td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
function fillData(id){
	var link = "<?php echo base_url('laporan/c_laporan/LaporanPeminjamanPerbulan');?>";
	$.ajax({
		type:'POST',
		url:''+link+'',
		data:{key:id},
		success:function(response){
			$('#targetDetail').html(response);
		}
	});
}
</script>