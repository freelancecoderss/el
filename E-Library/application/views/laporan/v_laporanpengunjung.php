<div class="panel-heading w3-teal">Laporan Pengunjung</div>
<div class="panel-body w3-white">
	<div class="form-group">
		<label>Urutkan Laporan Berdasarkan:</label>
		<select  class="form-control" id="idBulan" onchange="fillData(this.value)">
			<option selected disabled>-- Pilih --</option>
			<option value="" selected="selected">Laporan Perbulan</option>
			<?php foreach ($bulan as $dt): ?>
				<option value="<?=$dt->idBulan.'-'.$dt->tahun?>"><?php echo $dt->namaBulan.' - '.$dt->tahun;?></option>
			<?php endforeach ?>
		</select>
	</div>
	<div id="targetDetail" class="form-group">
		<table class="table">
			<thead>
				<tr>
					<th>No</th>
					<th>Bulan - Tahun</th>
					<th>Banyak Pengunjung</th>
				</tr>
			</thead>
			<tbody>
			<?php $no=1; foreach ($pengunjungPerBulan as $dt):?>
				<tr>
					<td><?php echo $no++;?></td>
					<td><?php echo $dt->bulan;?></td>
					<td><?php echo $dt->jumlah;?></td>
					<td><?php?></td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
function fillData(id){
	var link = "<?php echo base_url('laporan/c_laporan/LaporanPengunjungPerhari');?>";
	$.ajax({
		type:'POST',
		url:''+link+'',
		data:{key:id},
		success:function(response){
			$('#targetDetail').html(response);
		}
	});
}
</script>