<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>E-Library | Lihat  Data Buku Page</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
      <div class="warapper" style="padding-top:25px;">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                <div><?php echo $this->session->flashdata('pesan'); ?></div>
                    <div class="col-lg-12" align="center">
                        <div class="panel panel-default">
                          <div class="panel-heading" style="background-color: teal;color: white;text-align: left;"><b>Form Lihat Data Buku</b></div>
                            <div class="panel-body">
                                  <table class="table table-hover table-condensed">
                                    <tr style ="background-color: #ebebe0">
                                      <th style="text-align: center;">No</th>
                                      <th style="text-align: center;">Judul Buku</th>
                                      <th style="text-align: center;">Pengarang</th>
                                      <th style="text-align: center;">Penerbit</th>
                                      <th style="text-align: center;">Tahun Terbit</th>
                                      <th style="text-align: center;">ISBN</th>
									  <th style="text-align: center;">Ditambahkan Pada</th>
                                      <th style="text-align: center;">Aksi</th>
                                    </tr>
                                    <?php $no = 1; foreach ($data as $dt): ?>
                                    <tr>
                                      <td style="text-align: center;"><?php echo $no++;?></td>
                                      <td><span class="fa fa-book"><?php echo $dt->judul_buku;?></span></td>
                                      <td><?php echo $dt->pengarang;?></td>
                                      <td><?php echo $dt->penerbit;?></td>
                                      <td style="text-align: center;"><?php echo $dt->tahun_terbit;?></td>
                                      <td><?php echo $dt->isbn;?></td>
									  <td><?php echo $dt->tgl_input;?></td>
                                      <td>
                                      <center>
                                      <div id="thanks">
                                        <a class="btn btn-sm btn-primary" data-placement="bottom" data-toggle="tooltip" title="Edit Buku" href="<?php echo base_url('buku/c_halamanbuku/for_Edit/'.$dt->id_buku);?>"><span class="glyphicon glyphicon-edit"></span></a>
                                        <a class="btn btn-sm btn-danger tooltips" data-placement="bottom" data-toggle="tooltip" title="Hapus Buku" href="<?php echo base_url('buku/c_halamanbuku/Aksi_Hapus_data/'.$dt->id_buku);?>"><span class="glyphicon glyphicon-trash"></a>
									  </div>
									  </center>
									  </td>
									</tr>
                                    <?php endforeach ?> 
                                  </table>
                            </div>
                            <!-- panel body -->
                        </div>
                        <!-- panel default -->
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->
            </div>
     </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</body>
</html>
