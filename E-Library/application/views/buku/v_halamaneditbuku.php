<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>E-Library | Data Buku Page</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
      <div class="warapper" style="padding-top:25px;">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                <div><?php echo $this->session->flashdata('pesan'); ?></div>
                    <div class="col-lg-12" align="center">
                        <div class="panel panel-default">
                          <div class="panel-heading" style="background-color: teal;color: white;text-align: left;"><b>Form Edit Data Buku</b></div>
                            <div class="panel-body">
                                <form class="form-horizontal" style="" action="<?php echo base_url ('buku/c_halamanbuku/Aksi_Edit_Data');?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
                                  <div class="form-group">
                                      <!-- <label class="col-md-1 col-sm-1 control-label">Id Buku</label> -->
                                      <div class="col-sm-9">
                                          <input name="id" type="hidden" id="id" class="form-control" placeholder="" autofocus="on" required value="<?php echo $id_buku;?>">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-1 col-sm-1 control-label">Judul</label>
                                      <div class="col-sm-9">
                                          <input name="jdl" type="text" id="jdl" class="form-control" placeholder="Judul Buku" autofocus="on" required value="<?php echo $judul_buku;?>">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-1 col-sm-1 control-label">Pengarang</label>
                                      <div class="col-sm-9">
                                          <input name="pengarang" type="text" id="pengarang" class="form-control" placeholder="Nama Pengarang" autofocus="on" required value="<?php echo $pengarang;?>">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-1 col-sm-1 control-label">Penerbit</label>
                                      <div class="col-sm-9">
                                          <input name="penerbit" type="text" id="penerbit" class="form-control" placeholder="Nama Penerbit" autofocus="on" required value="<?php echo $penerbit;?>">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-1 col-sm-1 control-label">Tahun</label>
                                      <div class="col-sm-2">
                                          <input name="thn" type="number" id="thn" class="form-control" placeholder="Co: 2014" autofocus="on" required value="<?php echo $tahun_terbit;?>">
                                      </div>
                                      <form class="form-inline">
                                          <label class="col-md-1 col-sm-2 control-label">ISBN</label>
                                          <div class="col-sm-3">
                                              <input name="isbn" type="text" id="isbn" class="form-control" placeholder="co: 602-220-150-0" autofocus="on" required value="<?php echo $isbn;?>">
                                          </div>
                                      <label class="col-sm-11 col-sm-25 control-label"><br></label>
                                      <div class="col-sm-11">
                                        <button type="submit" name="ubah" class="btn btn-sm btn-primary"><span class="fa fa-floppy-o"></span> Ubah</button>
                                          <a href="<?=base_url('buku/c_halamanbuku/Tampilkan_Data');?>" class="btn btn-sm btn-danger">Batal &nbsp;<span class="fa fa-refresh"></span></a>
                                      </div>
                                      </form>
                                  </div>
                                </form>
                                <!-- form -->
                            </div>
                            <!-- panel body -->
                        </div>
                        <!-- panel default -->
                    </div>
                    <!-- col -->
                </div>
                <!-- row -->
            </div>
     </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</body>
</html>
