<?php echo $this->session->flashdata('pesan');?>
<div class="panel panel-deafault">
	<div class="panel-heading w3-teal">Data Peminjaman</div>
	<div class="panel-body">
	<?php $atribut = array("methode"=>"POST"); ?>
		<?php echo form_open("transaksi/c_peminjaman/SimpanDataPinjam", $atribut); ?>
			<div class="form-group">
				<label>ID Peminjaman:</label>
				<input type="text" name="id_pinjam" class="form-control" readonly value="<?php echo $lstid;?>">
			</div>
			<div class="form-group">
				<label>NIS/Nama Anggota:</label>
				<input type="text" name="id_anggota" class="form-control" id="id_anggota">
				<div id="sugestiA"></div>
				<!-- <select  class="form-control" name="id_anggota">
					<option selected disabled>-- Pilih --</option>
					<?php foreach ($dataAnggota as $da): ?>
						<option value="<?php echo $da->id_anggota;?>"><?php echo $da->no_induk.' - '.$da->nama;?></option>
					<?php endforeach ?>
				</select> -->
			</div>
			<div class="form-group">
				<label>Buku Di Pinjam (Judul Buku/ISBN):</label>
				<table id="dataDipinjam" class="table">
				</table>
				<table style="width: 100%;">
					<tr>
						<td>
							<!-- <select  class="form-control" id="idBuku" onchange="getBuku(this.value,this.options[this.selectedIndex].text)">
								<option selected disabled>-- Pilih --</option>
								<?php foreach ($dataBuku as $db): ?>
									<option value="<?php echo $db->id_buku;?>"><?php echo $db->judul_buku;?></option>
								<?php endforeach ?>
							</select> -->
							<input type="text" name="id_buku" class="form-control" id="id_buku" onchange="getBuku($(this).val())">
							<div id="sugestiB"></div>
						</td>
						<td>&nbsp;<button type="button" class="btn btn-default w3-blue" id="add-row"><span class="fa fa-plus"></span></button><button type="button" class="btn btn-warning" id="btn_hapus"><i class="fa fa-trash"></i></button></td>
					</tr>
				</table>
			</div>
			<div class="form-group">
				<label>Tanggal Kembali</label>
				<input type="date" name="tgl_kembali" class="form-control" placeholder="(tttt-bb-hh)">
			</div>
			<div>
				<button type="button" class="delete-row btn btn-default w3-red">Batal Pinjam</button>
				<button type="submit" class="btn btn-info">Simpan <i class="fa fa-floppy-o"></i></button>
				<!-- <a href="<?php echo base_url('transaksi/c_peminjaman/expdf');?>" class="btn btn-default w3-red" target="_blank">Print PDF <i class="fa fa-file-pdf-o"></i></a> -->
			</div>
	</div>
</div>

<?php echo form_close(); ?>
<script type="text/javascript">
	var dtBuku;
    var judul;
	var markup;
	
	$('#id_anggota').keyup(function(ev){
		var key = $(this).val();
		var link = '<?php echo base_url('transaksi/c_peminjaman/dataAnggota');?>'
		if (key != '') {
			$.ajax({
				type:'POST',
				url:link,
				data:{key:key},
				success:function(data){
					$('#sugestiA').fadeIn();
					$('#sugestiA').html(data);
				}
			});
		}else{
			$('#sugestiA').fadeOut();	
		}
	});
	$('#sugestiA').on('click','li',function(){
		$('#id_anggota').val($(this).text());
		$('#sugestiA').fadeOut();
	})

	$('#id_buku').keyup(function(ev){
		var key = $(this).val();
		var link = '<?php echo base_url('transaksi/c_peminjaman/dataBuku');?>'
		if (key != '') {
			$.ajax({
				type:'POST',
				url:link,
				data:{key:key},
				success:function(data){
					$('#sugestiB').fadeIn();
					$('#sugestiB').html(data);
				}
			});
		}else{
			$('#sugestiB').fadeOut();	
		}
	});
	$('#sugestiB').on('click','li',function(){
		$('#id_buku').val($(this).text());
		$('#sugestiB').fadeOut();
	})
	$('#id_anggota').focusout(function(){
		$('#sugestiA').fadeOut();
	});
	$('#id_buku').focusout(function(){
		$('#sugestiB').fadeOut();
	});
    // function getBuku(dtB){
    // 	dtBuku = dtB;
    // 	alert(dtBuku);
    // 	//judul = judulb;
    // 	markup = "<tr><td><input type='hidden' name='idBuku[]' class='form-control' id='dipinjam' value='"+dtBuku+"' readonly>"+dtBuku+"</td><td><button type='button' class='btn btn-warning'><input type='checkbox' name='hapus'></button></td></tr>";
    // }
    $('#add-row').click(function(ev){
    	dtBuku = $('#id_buku').val();
    	//alert(dtBuku);
    	$('#dataDipinjam').append("<tr><td><input type='hidden' name='idBuku[]' class='form-control' id='dipinjam' value='"+$('#list_idBuku').val()+"' readonly>"+dtBuku+"</td><td><button type='button' class='btn btn-warning'><input type='checkbox' name='hapus'></button></td></tr>");
    	$('#id_buku').val('');
    });
    $("#btn_hapus").click(function(){
            $("#dataDipinjam").find('input[name="hapus"]').each(function(){
                if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });

</script>
