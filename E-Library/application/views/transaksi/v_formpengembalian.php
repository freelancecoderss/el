<?php echo $this->session->flashdata('pesan');?>
<div class="panel panel-deafault">
	<div class="panel-heading w3-teal">Data Peminjaman</div>
	<div class="panel-body">
		<?php echo form_open("transaksi/c_pengembalian/SimpanDataPengembalian", "POST"); ?>
			<div class="form-group">
				<label>ID Pengemblian:</label>
				<input type="text" name="id_kembali" class="form-control" readonly id="id_kembali">
			</div>
			<div class="form-group">
				<label>ID Peminjaman:</label>
				<!-- <select  class="form-control" name="id_pinjam" id="id_pinjam">
					<option selected disabled>-- Pilih --</option>
					<?php foreach ($id_pinjam as $da): ?>
						<option value="<?php echo $da->id_pinjam;?>"><?php echo $da->id_pinjam;?></option>
					<?php endforeach ?>
				</select> -->
				<input type="text" name="id_pinjam" id="id_pinjam" class="form-control">
				<div id="sugestiA"></div>
			</div>
			<div class="form-group">
				<label>Nama Anggota:</label>
				<input type="text" name="id_anggota" class="form-control" readonly id="id_anggota">
			</div>
			<div class="form-group">
				<label>Buku Di Kembalikan:</label>
				<table id="dataDikembalikan" class="table">
				</table>
				<table style="width: 100%;">
					<tr>
						<td>
						<select  class="form-control" disabled id="id_buku" onchange="getBuku(this.value,this.options[this.selectedIndex].text)">
							<option selected disabled>-- Pilih --</option>
						</select>
						</td>
						<td>&nbsp; <button class="btn btn-primary btn-sm" type="button" id="tambahBuku"><i class="fa fa-plus"></i></button>&nbsp;<button type="button" class="btn btn-warning" id="btn_hapus"><i class="fa fa-trash"></i></button></td>
					</tr>	
				</table>
			</div>
			<!-- <div class="form-group">
				<label>Tanggal Kembali</label>
				<input type="date" name="tgl_kembali" class="form-control" placeholder="(tttt-bb-hh)">
			</div> -->
			<div>
				<button type="submit" class="btn btn-info">Simpan <i class="fa fa-floppy-o"></i></button>
			</div>
			<div id="target"></div>
		<?php echo form_close(); ?>
	</div>
</div>
<script type="text/javascript">
	var dataPeminjam ='';
	var id;
    var judul;
	var markup;
    function getBuku(idb,judulb){
    	id = idb;
    	judul = judulb;
    	markup = "<tr><td><input type='hidden' name='idBuku[]' class='form-control' id='dipinjam'' value='"+id+"' readonly>"+judul+"</td><td><button type='button' class='btn btn-warning'><input type='checkbox' name='hapus'></button></td></tr>";
    }
	$(document).ready(function(){
		$('#tambahBuku').click(function(ev){
			ev.preventDefault();
			$('#dataDikembalikan').append(markup);
			$('#id_buku').val('');
		});
	});
	$("#btn_hapus").click(function(){
            $("#dataDikembalikan").find('input[name="hapus"]').each(function(){
                if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });
	$('#id_pinjam').focusout(function(ev){
			ev.preventDefault();
			var key = $('#id_pinjam').val();
			var link = '<?php echo base_url('transaksi/c_pengembalian/fillDataKembali');?>';
			$.ajax({
				type:'POST',
				url:''+link+'',
				data:'key='+key+'',
				dataType:'json',
				success:function(response){
					$('#id_buku').empty();
					$('#id_buku').append("<option disabled selected>--Pilih--</option>");
					dataPeminjam = response.dataPeminjam;
					$('#id_anggota').val(response.dataPeminjam[0]['nama']);
					$('#id_kembali').val(response.id_pengembalian);
					for (var i = 0; i < dataPeminjam.length; i++) {
						$('#id_buku').removeAttr('disabled');
						$('#id_buku').append("<option value='"+dataPeminjam[i]['id_buku']+"'>"+dataPeminjam[i]['judul_buku']+"</option>");
						$('#sugestiA').fadeOut();
					}
				}
			});
		});
	$('#id_pinjam').keyup(function(ev){
		var key = $(this).val();
		var link = '<?php echo base_url('transaksi/c_pengembalian/idPengembalian');?>'
		if (key != '') {
			$.ajax({
				type:'POST',
				url:link,
				data:{key:key},
				success:function(data){
					$('#sugestiA').fadeIn();
					$('#sugestiA').html(data);
				}
			});
		}else{
			$('#sugestiA').fadeOut();	
		}
	});
	$('#sugestiA').on('click','li',function(){
		$('#id_pinjam').val($(this).text());
		$('#sugestiA').fadeOut();
	})
</script>