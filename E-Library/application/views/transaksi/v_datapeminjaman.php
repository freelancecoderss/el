<?php echo $this->session->flashdata('pesan');?>
<div class="panel panel-deafault">
	<div class="panel-heading w3-teal">Data Peminjaman</div>
	<div class="panel-body">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>ID Peminjaman</th>
					<th>Nama Anggota</th>
					<th>Tangal Pinjam</th>
					<th>Info</th>
				</tr>
			</thead>
			<tbody>
			<?php $noH = 1;$noD = 1;foreach ($datapinjam as $dt): ?>
				<tr>
					<td><?php echo $noH++;?></td>
					<td><?php echo $dt->id_pinjam;?></td>
					<td><?php echo $dt->nama;?></td>
					<td><?php echo $dt->tanggal_pinjam;?></td>
					<td><a class="btn btn-info" id="idPinjam" data-toggle="modal" data-target="#ModalDetile" onclick="fillData('<?php echo $dt->id_pinjam;?>')"><i class="fa fa-list-alt"></i>Detail</a>
					</td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<div>
	<div class="modal fade" id="ModalDetile" idPinjam="<?php echo $dt->id_pinjam;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header w3-blue">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Detile Peminjaman <i style="float:right;"><?php echo $dt->id_pinjam;?></i></h4>
	      </div>
	      <div class="modal-body" id="targetDetail">
	      </div>
	    <div class="modal-footer">
	        <span class="btn-group">
	          <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="fa fa-remove"></span> Tutup</button>
	        </span>
	     </div>
		</div>
	  </div>
	</div>
</div>

<script type="text/javascript">
function fillData(id){
	var link = "<?php echo base_url('transaksi/c_peminjaman/fillDataTransaksi');?>";
	$.ajax({
		type:'POST',
		url:''+link+'',
		data:{key:id},
		success:function(response){
			$('#targetDetail').html(response);
		}
	});
}
</script>