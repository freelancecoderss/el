<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>E-Library | Master</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:teal;">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url();?>" style="color:white;"><i class="fa fa-book"></i> &nbsp; E-Library</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navba
            r-right top-nav" style="">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:white;"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-user"></i> Profile</a ></li>
                        <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-gear"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
                    <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-bar-chart-o"></i> Charts</a></li>
                    <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-table"></i> Tables</a></li>
                    <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-edit"></i> Forms</a></li>
                    <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-desktop"></i> Bootstrap Elements</a></li>
                    <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-wrench"></i> Bootstrap Grid</a></li>
                    <li><a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li><a href="<?php echo base_url();?>">Dropdown Item</a></li>
                            <li><a href="<?php echo base_url();?>">Dropdown Item</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i> Dropdown 2<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li><a href="<?php echo base_url();?>">Dropdown Item 2</a></li>
                            <li><a href="<?php echo base_url();?>">Dropdown Item 2</a></li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="<?php echo base_url();?>"><i class="fa fa-fw fa-file"></i> Blank Page</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper" style="min-height:650px;background-color: transparent;">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Blank Page<small>Subheading</small></h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a></li>
                            <li class="active"><i class="fa fa-file"></i> Blank Page</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</body>
</html>
