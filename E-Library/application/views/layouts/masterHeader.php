<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>E-Library | <?php if (isset($title)) { echo $title; }?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/sb-admin.css');?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/w3.css');?>" rel="stylesheet">
    <!-- Data Table -->
    <!-- <link href="<?php echo base_url('assets/datatable/css/jquery.dataTables.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/datatable/css/dataTables.bootstrap.min.css');?>" rel="stylesheet"> -->

    <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
    
    <!-- <script src="<?php echo base_url('assets/datatable/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('assets/datatable/js/dataTables.bootstrap.min.js');?>"></script> -->
</head>
<body style="background-image:url('<?php echo base_url('assets/images/bg.jpg');?>')">
    <div id="wrapper" style="">
