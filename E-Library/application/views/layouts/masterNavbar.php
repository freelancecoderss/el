<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:teal;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url();?>" style="color:white;font-size:25px;font-weight:bold;"><i class="fa fa-book"></i> &nbsp;E-Library</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav" style="">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:white;font-weight:bold;"><i class="fa fa-user"></i> <?php echo $this->session->userdata('useraktif');?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-user"></i> Profile</a ></li>
                <li><a href="<?php echo base_url();?>"><i class="fa fa-fw fa-gear"></i> Settings</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url('admin/c_dashboard/logout');?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="<?php if (isset($aktif_db)){ echo $aktif_db;}?>"><br>
              <center style="" class="media-middle"><img src="<?php echo base_url($this->session->userdata('gambar'));?>" alt="user_pic" width="90px" class="img-circle"><br><br>
              <strong style="color:white;"><?php echo $this->session->userdata('useraktif');?></strong></center>
              <br>
              <a href="<?php echo base_url();?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            <li class="<?php if (isset($aktif_adm)){ echo $aktif_adm;}?>"><a href="javascript:;" data-toggle="collapse" data-target="#admin"><i class="fa fa-fw fa-user"></i> Admin <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="admin" class="collapse">
                    <li><a href="<?php echo base_url('admin/c_admin/ShowDataAdmin');?>">Data Admin</a></li>
                    <li><a href="<?php echo base_url('admin/c_admin/ShowInputAdmin');?>">Tambah Admin</a></li>
                </ul>
            </li>
            <li class="<?php if (isset($aktif_ag)){ echo $aktif_ag;}?>"><a href="javascript:;" data-toggle="collapse" data-target="#anggota"><i class="fa fa-fw fa-users"></i> Anggota <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="anggota" class="collapse">
                    <li><a href="<?php echo base_url('anggota/c_anggota/Form_Data_Show');?>">Data Anggota</a></li>
                    <li><a href="<?php echo base_url('anggota/c_anggota/Form_Input_Show');?>">Tambah Anggota</a></li>
                </ul>
            </li>
            <li class="<?php if (isset($aktif_bku)){ echo $aktif_bku;}?>"><a href="javascript:;" data-toggle="collapse" data-target="#buku"><i class="fa fa-fw fa-book"></i> Buku <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="buku" class="collapse">
                    <li><a href="<?php echo base_url('buku/c_halamanbuku/Tampilkan_Data');?>">Data Buku</a></li>
                    <li><a href="<?php echo base_url('buku/c_halamanbuku/Form_Input_Show');?>">Tambah Buku</a></li>
                </ul>
            </li>
            <li class="<?php if (isset($aktif_trns)){ echo $aktif_trns;}?>"><a href="javascript:;" data-toggle="collapse" data-target="#transaksi"><i class="fa fa-fw fa-list-alt"></i> Transaksi <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="transaksi" class="collapse">
                    <li><a href="<?php echo base_url('transaksi/c_peminjaman/index');?>">Data Transaksi</a></li>
                    <li><a href="<?php echo base_url('transaksi/c_peminjaman/FormPeminjaman');?>">Peminjaman</a></li>
                    <li><a href="<?php echo base_url('transaksi/c_pengembalian/FormPengembalian');?>">Pengembalian</a></li>
                </ul>
            </li>
            <li class="<?php if (isset($aktif_lap)){ echo $aktif_lap;}?>"><a href="javascript:;" data-toggle="collapse" data-target="#laporan"><i class="fa fa-fw fa-bar-chart-o"></i> Laporan <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="laporan" class="collapse">
				<li><a href="<?php echo base_url('laporan/c_laporan/LaporanPengunjung');?>">Pengunjung</a></li>
                    <li><a href="<?php echo base_url('laporan/c_laporan/LaporanPeminjaman');?>">Peminjaman</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>
<div id="page-wrapper" style="min-height:650px;background-color: transparent;">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header"><?php if (isset($menu)){ echo $menu;}?></h1>
              <ol class="breadcrumb">
              		<li><i class="fa fa-dashboard"></i> <?php if (isset($menu)){ echo $menu;}?></li>
                    <li class="<?php if(isset($aktif_sub1)){echo $aktif_sub1;}?>"><?php if (isset($sub1)){ echo "<i class='fa fa-file'></i> ".$sub1;}?></li>
              		<li class="<?php if(isset($aktif_sub2)){echo $aktif_sub2;}?>"></i> <?php if (isset($sub2)){ echo "<i class='fa fa-file'></i> ".$sub2;}?></li>
              </ol>
