<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_halamanbuku extends CI_Controller {

	public function index()
	{
		if ($this->session->has_userdata('session_key')) 
		{
			redirect('/admin/dashboard');
		}
		else
		{
			$hasil = $this->pengunjung->HariIni();
			$countData = count($hasil);
			$akumulasi = $this->pengunjung->AkumulasiPengunjung();
			$countAkumulasi = count($akumulasi);
			print_r($countAkumulasi);
			$data = array(
				'dataHariIni' 	=> $hasil,
				'jmlHariIni' 	=> $countData,
				'akumulasi' 	=> $akumulasi,
				'countAkumulasi'=> $countAkumulasi,
				);

			$this->load->view('v_halamanutama', $data);
		}
	}

	public function Form_Input_Show()
	{
		if ($this->session->has_userdata('session_key'))
		{
			$data = array
			(
				'title'=>'Tambah Data Buku',
				'aktif_bku'=>'active',
				'aktif_menu'=>'active',
				'menu' 		=> 'Buku',
				'sub1' 		=> 'Data Buku',
				'sub2' 		=> 'Input Data Buku',
			);
			$this->load->view('layouts/masterHeader',$data);
			$this->load->view('layouts/masterNavbar',$data);
			$this->load->view('buku/v_halamaninputbuku');
			$this->load->view('layouts/masterFooter');
		}
		else
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('landing/loadFormLogin');
		}
	}

	public function Form_Lihat()
	{

		if ($this->session->has_userdata('session_key')) 
		{
			redirect('/buku/databuku/Lihat_Data');
		}
		else
		{
			$this->load->view('v_halaman_login');
		}
	}

	public function Tampilkan_Data()
	{
		$dataHalaman = array
			(
				'title'=>'Lihat Data Buku',
				'aktif_bku'=>'active',
				'aktif_menu'=>'active',
				'menu' 		=> 'Buku',
				'sub1' 		=> 'Data Buku',
			);

		$hasil = $this->buku->AmbilData();
		$data = array
		(
			'data' => $hasil
		);

		$this->load->view('layouts/masterHeader',$dataHalaman);
		$this->load->view('layouts/masterNavbar',$dataHalaman);
		$this->load->view('buku/v_halamanlihatbuku', $data);
		$this->load->view('layouts/masterFooter', $data);
	}

	public function Aksi_Tambah_Data()
	{
		$data = array('judul_buku' =>  $_POST['jdl'],
			'pengarang' => $_POST['pengarang'],
			'penerbit' => $_POST['penerbit'],
			'tahun_terbit' => $_POST['thn'],
			'isbn' => $_POST['isbn']
			);

		$hasil = $this->buku->InsertData($data);

		if ($hasil) 
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil diinputkan</div>');
			redirect(base_url('buku/c_halamanbuku/Tampilkan_Data'));
		}
		else
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Data Gagal diinputkan</div>');
			redirect(base_url('buku/c_halamanbuku/Form_Input_Show'));
		}
	}

	public function for_Edit($key)
	{
		$result = $this->buku->GetWhere($key);
		$data = array(
			'id_buku'=> $result[0] -> id_buku,
			'judul_buku' => $result[0] -> judul_buku,
			'pengarang' => $result[0] -> pengarang,
			'penerbit' => $result[0] -> penerbit,
			'tahun_terbit' => $result[0] -> tahun_terbit,
			'isbn' => $result[0] -> isbn
			);

		$datab = array
		(
			'title'=>'Edit Data Buku',
			'aktif_bku'=>'active',
			'aktif_menu'=>'active',
			'menu' 		=> 'Buku',
			'sub1' 		=> 'Data Buku',
			'sub2' 		=> 'Edit Data Buku',
		);

		$this->load->view('layouts/masterHeader',$datab);
		$this->load->view('layouts/masterNavbar',$datab);
		$this->load->view('buku/v_halamaneditbuku', $data);
		$this->load->view('layouts/masterFooter');
	}

	public function Aksi_Edit_Data()
	{
		$data = array( 'id_buku' => $_POST['id'],
			'judul_buku' =>  $_POST['jdl'],
			'pengarang' => $_POST['pengarang'],
			'penerbit' => $_POST['penerbit'],
			'tahun_terbit' => $_POST['thn'],
			'isbn' => $_POST['isbn']
			);

		$key = array('id_buku' => $_POST['id']);
		$hasil = $this->buku->EditData($data, $key);

		if ($hasil) 
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil diubah</div>');
			redirect(base_url('buku/c_halamanbuku/Tampilkan_Data'));
		}
		else
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Data Gagal diubah</div>');
			redirect(base_url('buku/c_halamanbuku/for_Edit'));
		}
	}

	public function Aksi_Hapus_data($key)
	{
		$data = array
		(
			'id_buku' => $key,
			
		);
		
		$result = $this->buku->HapusData($data);
		if ($result) 
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil dihapus</div>');
			redirect(base_url('buku/c_halamanbuku/Tampilkan_Data'));
		}
		else
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Data gagal berhasil dihapus</div>');	
			redirect(base_url('buku/c_halamanbuku/Tampilkan_Data'));
		}
	}
}
