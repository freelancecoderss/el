<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_admin extends CI_Controller {
	
	public function ShowInputAdmin()
	{
		if ($this->session->has_userdata('session_key')){
			$data = array(
				'title'=>'Admin',
				'aktif_adm'=>'active',
				'aktif_sub2'=>'active',
				'menu' => 'Administrator',
				'sub1' => 'Data Admin',
				'sub2' => 'Tambah Data Admin',
			);
			$this->load->view('layouts/masterHeader',$data);
			$this->load->view('layouts/masterNavbar');
			$this->load->view('admin/v_tambahadmin');
			$this->load->view('layouts/masterFooter');
		}else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}
	
	public function InsertAdmin()
	{
		$this->load->library('upload');
		
		$config['upload_path']          = './assets/images/admin/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		
		$this->upload->initialize($config);
		
		$foto = substr($config['upload_path'],2).$_FILES["gambar"]["name"];
		
		if(!$this->upload->do_upload("gambar"))
		{
			$this->session->set_flashdata('pesan', $this->upload->display_errors());
			redirect(base_url('admin/c_admin/ShowInputAdmin'));
		} 
		else
		{
			$data = array(
			'nama' => $_POST['nama'],
			'username' => $_POST['username'],
			'password' => $_POST['password'],
			'foto' => $foto);
			
			$result = $this->admin->InsertData($data);
			
			if($result)
			{
				$this->session->set_flashdata('pesan','<div class="alert alert-success">data berhasil ditambahkan</div>');
				redirect(base_url('admin/c_admin/ShowInputAdmin'));
			}
			else
			{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger">data gagal diinsert</div>');
				redirect(base_url('admin/c_admin/ShowInputAdmin'));
			}
		}  
	
	}
	
	public function ShowDataAdmin()
	{
		if ($this->session->has_userdata('session_key')){
			$data = array(
				'title'=>'Admin',
				'aktif_adm'=>'active',
				'menu' => 'Administrator',
				'sub1' => 'Data Admin',
				'aktif_sub1'=>'active',
			);
			$this->load->view('layouts/masterHeader',$data);
			$this->load->view('layouts/masterNavbar');
			$dataDb = $this->admin->AmbilData();
			$data = array(
				'data'=>$dataDb,
			);
			$this->load->view('admin/v_dataadmin', $data);
			$this->load->view('layouts/masterFooter');
		}else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}
	
	public function ShowFormEdit($key)
	{
		if ($this->session->has_userdata('session_key')){
			$data = array(
				'title'=>'Admin',
				'aktif_adm'=>'active',
				'aktif_sub2'=>'active',
				'menu' => 'Administrator',
				'sub1' => 'Data Admin',
				'sub2' => 'Ubah Data Admin',
			);
			$this->load->view('layouts/masterHeader',$data);
			$this->load->view('layouts/masterNavbar');
			
			$result = $this->admin->GetWhere($key);
			
			$data = array(
			'id_admin'=>$result[0]->id_admin,
			'nama'=>$result[0]->nama,
			'username'=>$result[0]->username,
			'password'=>$result[0]->password,
			'foto'=>$result[0]->foto,
			);
			
			$this->load->view('admin/v_editadmin', $data);
			$this->load->view('layouts/masterFooter');
		}else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}
	
	public function UpdateAdmin()
	{
		if($_FILES["gambar"]["name"] != "")
		{
			$this->load->library('upload');
			
			$config['upload_path']          = './assets/images/admin/';
			$config['allowed_types']        = 'gif|jpg|jpeg|png';
			
			$this->upload->initialize($config);
			
			$foto = substr($config['upload_path'],2).$_FILES["gambar"]["name"];
			
			if(!$this->upload->do_upload("gambar"))
			{
				$this->session->set_flashdata('pesan', $this->upload->display_errors());
				redirect(base_url('admin/c_admin/ShowDataAdmin'));
			}
			else
			{
				$data = array(
				'nama'=>$_POST['nama'],
				'username'=>$_POST['username'],
				'password'=>$_POST['password'],
				'foto'=>$foto
				);
				
				$key=array('id_admin' => $_POST['id_admin']);
				
				$result = $this->admin->UpdateData($data, $key);
				
				if($result)
				{
					$this->session->set_flashdata('pesan','<div class="alert alert-success">data berhasil diubah</div>');
					redirect(base_url('/admin/c_admin/ShowDataAdmin'));
				}
				else
				{
					$this->session->set_flashdata('pesan','<div class="alert alert-danger">data gagal diubah</div>');
					redirect(base_url('admin/c_admin/ShowDataAdmin'));
				}
			}
			
		}
		else
		{
			$data = array(
			'nama'=>$_POST['nama'],
			'username'=>$_POST['username'],
			'password'=>$_POST['password']
			);
			
			$key=array('id_admin' => $_POST['id_admin']);
			
			$result = $this->admin->UpdateData($data, $key);
			
			if($result)
			{
				$this->session->set_flashdata('pesan','<div class="alert alert-success">data berhasil diubah</div>');
				redirect(base_url('admin/c_admin/ShowDataAdmin'));
			}
			else
			{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger">data gagal diubah</div>');
				redirect(base_url('admin/c_admin/ShowDataAdmin'));
			}
		}
	}
	
	public function DeleteAdmin($key)
	{
		$data = array(
			'id_admin'=>$key
		);
		
		$result = $this->admin->DeleteData($data);
		
		if($result){
			$this->session->set_flashdata('pesan','<div class="alert alert-success">data berhasil dihapus</div>');
			redirect(base_url('admin/c_admin/ShowDataAdmin'));
		}
		else{
			$this->session->set_flashdata('pesan','<div class="alert alert-danger">data gagal dihapus</div>');
			redirect(base_url('admin/c_admin/ShowDataAdmin'));
		}
	}
}
