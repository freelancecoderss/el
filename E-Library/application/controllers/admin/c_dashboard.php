<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_dashboard extends CI_Controller {
	
	public function index()
	{
		if ($this->session->has_userdata('session_key')){

			$pengunjungHariIni = $this->pengunjung->HariIni();
			$jmlPengunjung = count($pengunjungHariIni);
			$akumulasi = count($this->pengunjung->AkumulasiPengunjung());
			$buku = count($this->buku->AmbilData());
			$jmlanggota = count($this->anggota->AmbilData());
			$anggotaBaru = $this->anggota->AnggotaBaru();
			$bukuBaru = $this->buku->BukuBaru();
			$peminjamanHarusKembali = $this->peminjaman->GetPeminjamanHarusKembali();
			$data = array(
				'title'				=>'Dashboard',
				'aktif_db'			=>'active',
				'aktif_menu'		=>'active',
				'menu' 				=> 'Dashboard',
				'sub1' 				=> 'Welcome',
				'pengunjung'		=> $pengunjungHariIni,
				'jmlHariIni'		=> $jmlPengunjung,
				'akumulasi' 		=> $akumulasi,
				'jmlBuku'			=> $buku,
				'jmlAnggota'		=> $jmlanggota,
				'anggotaBaru'		=> $anggotaBaru,
				'bukuBaru'			=> $bukuBaru,
				'peminjamanHarusKembali'	=> $peminjamanHarusKembali
			);
			$this->load->view('layouts/masterHeader', $data);
			$this->load->view('layouts/masterNavbar');
			$this->load->view('admin/v_dashboard', $data);
			$this->load->view('layouts/masterFooter');
		}else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}
	
	public function detailBukuHarusKembali()
	{
		$key = $_POST['key'];
		$detailHarusKembali = $this->peminjaman->getDetailHarusKembali($key);
		
		$data = array(
			'detailHarusKembali' 	=> $detailHarusKembali,
		);
		return $this->load->view('admin/v_detailharuskembali', $data);
	}
	
	public function logout()
	{
		$this->session->unset_userdata('session_key','useraktif', 'gambar','__ci_last_regenerate');
		redirect('c_halamanutama/index');
	}
	
	public function CobaEmail()
	{
		$this->load->library('email');
		
		/* $config = array(
			'mailtype' => 'html',
			'protocol' => 'smtp',
			'smtp_host' => 'smtp-relay.gmail.com',
			'smtp_user' => 'sother77@gmail.com',
			'smtp_password'=> '77_Zaenal',
			'smtp_port' => 25
		); */
		
		//$this->email->initialize($config);
		$this->email->from('sother77@gmail.com', 'zaenal');
		$this->email->to('deaarya15@gmail.com');
		$this->email->subject('uji coba email ci');
		$this->email->message('segera kembalikan buku anda');
		
		if($this->email->send())
		{
			echo "berhasil hore, kita berhasil";
		}
		else
		{
			echo $this->email->print_debugger();
		}
			
	}
}
