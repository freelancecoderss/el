<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_halamanutama extends CI_Controller {

	public function index()
	{
		if ($this->session->has_userdata('session_key')) {
			redirect('/admin/c_dashboard');
		}else{

			$hasil= $this->pengunjung->HariIni();
			$countData = count($hasil);
			$akumulasi = $this->pengunjung->AkumulasiPengunjung();
			$countAkumulasi = count($akumulasi);
			$data = array(
				'dataHariIni' 	=> $hasil,
				'jmlHariIni' 	=> $countData,
				'akumulasi' 	=> $akumulasi,
				'countAkumulasi'=> $countAkumulasi,
				);

			$this->load->view('v_halamanutama', $data);
		}
	}
	public function loadFormLogin()
	{
		if ($this->session->has_userdata('session_key')) {
			redirect('/admin/c_dashboard');
		}else{
			$this->load->view('v_halamanlogin');
		}
	}
	
	public function InsertPengunjung()
	{
		$id_pengunjung = $this->pengunjung->getLastID();
		
		$data = array(
		'id_pengunjung' => $id_pengunjung,
		'no_induk' => $_POST['nis'],
		'nama' => $_POST['nama'],
		'keperluan' => $_POST['keperluan']
		);
		
		echo $id_pengunjung;
		echo '<br>';
		echo $_POST['nis'];
		echo '<br>';
		echo $_POST['nama'];
		echo '<br>';
		echo $_POST['keperluan'];
		
		$result = $this->pengunjung->InsertData($data);
		
		if($result)
		{
			$this->session->set_flashdata('pesan','<div class="alert alert-success">data berhasil ditambahkan</div>');
			redirect(base_url('c_halamanutama/index'));
		}
		else
		{
			$this->session->set_flashdata('pesan','<div class="alert alert-danger">data gagal ditambahkan</div>');
			redirect(base_url('c_halamanutama/index'));
		}
	}
	public function CekLogin()
	{
		$user = $this->input->post('user');
		$pwd = $this->input->post('pwd');

		$hasil = $this->admin->VerfLogin($user,$pwd);

		if ($hasil) {
			$user_data = array(
				'session_key' => md5(time()),
				'useraktif' => $hasil[0]->nama,
				'gambar' => $hasil[0]->foto,
			);
			$this->session->set_userdata($user_data);
			redirect('/admin/c_dashboard');
		}else {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Maaf Username / Password Anda Salah!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}
}
