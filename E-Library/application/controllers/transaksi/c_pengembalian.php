<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_pengembalian extends CI_Controller {

	public function index()
	{
		if ($this->session->has_userdata('session_key')){
			$data = array(
				'title'			=> 'Pengembalian',
				'aktif_trns'	=> 'active',
				'aktif_sub1'	=> 'active',
				'menu' 			=> 'Pengembalian',
				'sub1' 			=> 'Data Pengembalian',
			);
			$this->load->view('layouts/masterHeader', $data);
			$this->load->view('layouts/masterNavbar');
			$this->load->view('transaksi/v_datapengembalian');
			$this->load->view('layouts/masterFooter');
		}else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}
	public function FormPengembalian()
	{	
		$lstid = $this->pengembalian->getLastID();
		$id_pinjam = $this->peminjaman->getIdPinjam();
		$dataAnggota = $this->anggota->AmbilData();
		
		$data = array(
				'title'			=> 'Pengembalian',
				'aktif_trns'	=> 'active',
				'aktif_sub2'	=> 'active',
				'menu' 			=> 'Pengembalian',
				'sub1' 			=> 'Data Pengembalian',
				'sub2' 			=> 'Input Pengembalian',
				'lstid'			=> $lstid,
				'id_pinjam'		=> $id_pinjam,
			);
			
		$this->load->view('layouts/masterHeader', $data);
		$this->load->view('layouts/masterNavbar');
		$this->load->view('transaksi/v_formpengembalian');
		$this->load->view('layouts/masterFooter');
	}
	public function idPengembalian()
	{
		$key = $_POST['key'];
		$data = $this->peminjaman->GetLike($key);
		//$data = json_encode($data);
		$hasil = "<ul style='list-style-type:none; padding:0;background-color:#eee;cursor:pointer;'>";
		if (count($hasil) > 0) {
			foreach ($data as $dt) {
				$hasil .= "<li style='padding:10px;'>".$dt->id_pinjam."</li>";
			}
		}else{
			$hasil .="<li style='padding:10px;'>Data Tidak Ditemukan!</li>";
		}
		echo $hasil .="</ul>";
	}
	public function fillDataKembali()
	{
		$dataPeminjam = $this->peminjaman->getDataPeminjam($this->input->post('key'));
		
		$data = json_encode([
				'dataPeminjam'		=> $dataPeminjam['data'],
				'id_pengembalian'	=> $dataPeminjam['id_pengembalian']
			]);
		echo $data;
	}
	public function SimpanDataPengembalian()
	{
		$id_kembali = $this->input->post('id_kembali');
		$id_pinjam = $this->input->post('id_pinjam');
		$id_buku = $this->input->post('idBuku');
		$tgl_kembali = date('Y-m-d H:i:s');
		$dataHeader = array(
				'id_pengembalian'	=> $id_kembali,
				'id_pinjam'			=> $id_pinjam,
				);
		$cekIdPengemblaian = $this->pengembalian->GetLike($id_kembali);
		
		if(count($cekIdPengemblaian) > 0)
		{
			for ($i=0; $i <count($id_buku) ; $i++)
			{ 
				$dataDetail = array(
					'id_pengembalian'	=> $id_kembali,
					'id_buku'		=> $id_buku[$i],
					'tanggal_dikembalikan'	=> $tgl_kembali,
					);
				$SimpanDetail = $this->pengembalian->SimpanDetailKembali($dataDetail);
			}
			if ($SimpanDetail) {
				$this->session->set_flashdata('pesan', '<div class="alert alert-success">Data Berasil Disimpan.</div>');
				redirect('transaksi/c_peminjaman');
			}else{
				$this->session->set_flashdata('pesan', '<div class="alert alert-warning">Terjadi Kesalahan 2 Saat Mencoba Menyimpan!Terjadi Kesalahan 1 Saat Mencoba Menyimpan!</div>');
				redirect('transaksi/c_peminjaman/FormPeminjaman');
			}
		}
		else
		{
			$SimpanHeader = $this->pengembalian->SimpanHeaderKembali($dataHeader);
			if ($SimpanHeader) {
				for ($i=0; $i <count($id_buku) ; $i++) { 
					$dataDetail = array(
						'id_pengembalian'	=> $id_kembali,
						'id_buku'		=> $id_buku[$i],
						'tanggal_dikembalikan'	=> $tgl_kembali,
						);
					$SimpanDetail = $this->pengembalian->SimpanDetailKembali($dataDetail);
				}
				if ($SimpanDetail) {
					$this->session->set_flashdata('pesan', '<div class="alert alert-success">Data Berasil Disimpan.</div>');
					redirect('transaksi/c_peminjaman');
				}else{
					$this->session->set_flashdata('pesan', '<div class="alert alert-warning">Terjadi Kesalahan 2 Saat Mencoba Menyimpan!Terjadi Kesalahan 1 Saat Mencoba Menyimpan!</div>');
					redirect('transaksi/c_peminjaman/FormPeminjaman');
				}
			}else{
				$this->session->set_flashdata('pesan', '<div class="alert alert-warning">Terjadi Kesalahan 1 Saat Mencoba Menyimpan!</div>');
					redirect('transaksi/c_peminjaman/FormPeminjaman');
			}
		}
	}
}
