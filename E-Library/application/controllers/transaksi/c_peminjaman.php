<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_peminjaman extends CI_Controller {

	public function __constract()
	{
		parent::__constract();
		$this->load->library('fpdf');
	}
	public function index()
	{
		if ($this->session->has_userdata('session_key')){
			$datapinjam = $this->peminjaman->AmbilHeaderData();
			$data = array(
				'title'			=> 'Peminjaman',
				'aktif_trns'	=> 'active',
				'aktif_sub1'	=> 'active',
				'menu' 			=> 'Peminjaman',
				'sub1' 			=> 'Data Peminjaman',
				'datapinjam'	=> $datapinjam,
			);
			$this->load->view('layouts/masterHeader', $data);
			$this->load->view('layouts/masterNavbar');
			$this->load->view('transaksi/v_datapeminjaman');
			$this->load->view('layouts/masterFooter');
		}else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}
	public function FormPeminjaman()
	{	
		$lstid = $this->peminjaman->getLastID();
		$dataBuku = $this->buku->AmbilData();
		$dataAnggota = $this->anggota->AmbilData();
		$data = array(
				'title'			=> 'Peminjaman',
				'aktif_trns'	=> 'active',
				'aktif_sub2'	=> 'active',
				'menu' 			=> 'Peminjaman',
				'sub1' 			=> 'Data Peminjaman',
				'sub2' 			=> 'Input Peminjaman',
				'dataBuku'		=> $dataBuku,
				'dataAnggota'	=> $dataAnggota,
				'lstid'			=> $lstid,
			);
			
		$this->load->view('layouts/masterHeader', $data);
		$this->load->view('layouts/masterNavbar');
		$this->load->view('transaksi/v_formpeminjaman');
		$this->load->view('layouts/masterFooter');
	}
	public function fillDataTransaksi()
	{
		$key = $_POST['key'];
		$detailPinjam = $this->peminjaman->getDetailPinjam($key);
		
		$data = array(
			'detailPinjam' 	=> $detailPinjam,
		);
		return $this->load->view('transaksi/v_detailpinjam', $data);
	}
	public function dataAnggota()
	{
		$key = $_POST['key'];
		$data = $this->anggota->GetLike($key);
		//$data = json_encode($data);
		$hasil = "<ul style='list-style-type:none; padding:0;background-color:#eee;cursor:pointer;'>";
		if (count($hasil) > 0) {
			foreach ($data as $dt) {
				$hasil .= "<li style='padding:10px;'>".$dt->id_anggota." / ".$dt->no_induk." / ".$dt->nama."</li>";
			}
		}else{
			$hasil .="<li style='padding:10px;'>Data Tidak Ditemukan!</li>";
		}
		echo $hasil .="</ul>";
	}
	public function dataBuku()
	{
		$key = $_POST['key'];
		$data = $this->buku->GetLike($key);
		//$data = json_encode($data);
		$hasil = "<ul style='list-style-type:none; padding:0;background-color:#eee;cursor:pointer;'>";
		if (count($hasil) > 0) {
			foreach ($data as $dt) {
				$hasil .= "<li style='padding:10px;'><input type='hidden' value='".$dt->id_buku."' name='list_idBuku' id='list_idBuku'>".$dt->judul_buku." / ".$dt->isbn."</li>";
			}
		}else{
			$hasil .="<li style='padding:10px;'>Data Tidak Ditemukan!</li>";
		}
		echo $hasil .="</ul>";
	}
	public function SimpanDataPinjam()
	{
		$id_pinjam 	= $this->input->post('id_pinjam');
			$id_anggota	= substr($this->input->post('id_anggota'),0,8);
			$tgl_kembali= $this->input->post('tgl_kembali');
			$id_buku	= $this->input->post('idBuku');

		$data = $this->peminjaman->fillPdfData($id_anggota,$id_buku);
		$this->session->set_flashdata('id_pinjam',$this->input->post('id_pinjam'));
		$this->session->set_flashdata('nAnggota',$data['nAnggota']);
		$this->session->set_flashdata('jBuku',$data['jBuku']);
		$this->session->set_flashdata('tgl_kembali',$this->input->post('tgl_kembali'));
		$dataHeader = array(
				'id_pinjam'			=> $id_pinjam,
				'id_anggota'		=> $id_anggota,
				'tanggal_pinjam'	=> date('Y-m-d'),
				'tanggal_kembali'	=> $tgl_kembali,
				);
		$SimpanHeader = $this->peminjaman->SimpanHeaderPinjam($dataHeader);
		if ($SimpanHeader) {
			for ($i=0; $i <count($id_buku) ; $i++) { 
				$dataDetail = array(
					'id_pinjam' => $id_pinjam,
					'id_buku'	=> $id_buku[$i],
					);
				$SimpanDetail = $this->peminjaman->SimpanDetailPinjam($dataDetail);
			}
			if ($SimpanDetail) {
				$this->session->set_flashdata('pesan', '<div class="alert alert-success">Data Berasil Disimpan.</div>');
				redirect('transaksi/c_peminjaman/loadPDF/'.$pdf);
			}else{
				$this->session->set_flashdata('pesan', '<div class="alert alert-warning">Terjadi Kesalahan 2 Saat Mencoba Menyimpan!Terjadi Kesalahan 1 Saat Mencoba Menyimpan!</div>');
				redirect('transaksi/c_peminjaman/FormPeminjaman');
			}
		}else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-warning">Terjadi Kesalahan 1 Saat Mencoba Menyimpan!</div>');
				redirect('transaksi/c_peminjaman/FormPeminjaman');
		}
	}
	public function loadPDF()
	{
		$this->session->keep_flashdata('id_pinjam');
		$this->session->keep_flashdata('nAnggota');
		$this->session->keep_flashdata('jBuku');
		$this->session->keep_flashdata('tgl_kembali');
		$data = array(
			'title'			=> 'Peminjaman',
			'aktif_trns'	=> 'active',
			'aktif_sub1'	=> 'active',
			'menu' 			=> 'Peminjaman',
			'sub1' 			=> 'Data Peminjaman',
			'sub2' 			=> 'Bukti Peminjaman',
		);
		$this->load->view('layouts/masterHeader', $data);
		$this->load->view('layouts/masterNavbar');
		$this->load->view('transaksi/v_invoicepinjam');
		$this->load->view('layouts/masterFooter');

	}
	public function expdf()
	{
		$id_pinjam = $this->session->flashdata('id_pinjam');
		$nama_anggota = $this->session->flashdata('nAnggota');
		$buku = $this->session->flashdata('jBuku');
		$tgl_pinjam = date('Y-m-d');
		$tgl_kembali = $this->session->flashdata('tgl_kembali');
		header('Content-type: application/pdf');
		$this->load->library('fpdf');
		$this->fpdf->AddPage('P','','A5');	
		$this->fpdf->SetFont('Arial','B',16);

		$link = base_url('assets/images/avatar5.png');
		$this->fpdf->Image($link,20,10, 25,25);
		
		$this->fpdf->Cell(189,5,'',0,1);

		$this->fpdf->Cell(49,8,'',0,0);
		$this->fpdf->Cell(20,8,'',0,0);
		$this->fpdf->Cell(75,8,'Perpustakaan Digital',0,1);//endline

		$this->fpdf->Cell(50,8,'',0,0);
		$this->fpdf->Cell(20,8,'',0,0);
		$this->fpdf->Cell(90,8,'SMA Negeri 2 Garut',0,1);

		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->Cell(40,5,'',0,0);
		$this->fpdf->Cell(90,5,'Jalan Raya Leles No. ?? Tlp. 0212 450123 Fax. 0212 450124',0,1);

		$this->fpdf->Cell(189,5,'',0,1);//new blank space
		$this->fpdf->Cell(189,0,'',1,1);//new line
		$this->fpdf->Cell(189,5,'',0,1);
		
		$this->fpdf->SetFont('Arial','B',13);
		$this->fpdf->Cell(50,7,'',0,0);
		$this->fpdf->Cell(15,10,'',0,0);
		$this->fpdf->Cell(90,7,'Bukti Peminjaman Buku',0,1);
		
		$this->fpdf->Cell(189,6,'',0,1);
		$this->fpdf->SetFont('Arial','',11);

		$this->fpdf->Cell(9,5,'',0,0);
		$this->fpdf->Cell(35,5,'ID Peminjaman',0,0);
		$this->fpdf->Cell(63,5,': '.$id_pinjam,0,0);
		$this->fpdf->Cell(47,5,'Tanggal Peminjaman',0,0);
		$this->fpdf->Cell(35,5,': '.$tgl_pinjam,0,1);//endline

		$this->fpdf->Cell(9,5,'',0,0);
		$this->fpdf->Cell(35,5,'Nama Anggota',0,0);
		$this->fpdf->Cell(63,5,': '.$nama_anggota,0,0);
		$this->fpdf->Cell(47,5,'Tanggal Pengembalian',0,0);
		$this->fpdf->Cell(35,5,': '.$tgl_kembali,0,1);//endline
		
		$this->fpdf->Cell(189,5,'',0,1);//new blank line

		$this->fpdf->Cell(32,6,'',0,0);
		$this->fpdf->Cell(120,6,'Daftar Buku yang Dipinjam:',0,1);

		$this->fpdf->Cell(33,7,'',0,0);
		$this->fpdf->Cell(20,6,'[No.]',1,0);
		$this->fpdf->Cell(100,6,'[Judul Buku]',1,1);
		for ($i=0; $i < count($buku); $i++) { 
			$no = $i+1;
			$this->fpdf->Cell(33,7,'',0,0);
			$this->fpdf->Cell(20,7,$no,1,0);
			$this->fpdf->Cell(100,7,$buku[$i],1,1);
		}

		$this->fpdf->Cell(189,7,'',0,1);//new blank line
		$this->fpdf->Cell(130,5,'',0,0);
		$this->fpdf->Cell(40,5,'Administrator Perpustakaan',0,1);		

		$this->fpdf->Cell(189,20,'',0,1);//new blank line
		$this->fpdf->Cell(130,5,'',0,0);
		$this->fpdf->Cell(50,0,'',1,1);
		$this->fpdf->Cell(130,5,'',0,0);
		$this->fpdf->Cell(40,5,'Administrator Perpustakaan',0,1);		
		
		ob_get_clean();
		$this->fpdf->Output('I','BuktiPeminjaman'.date('YmdHis'));
	}
}	
