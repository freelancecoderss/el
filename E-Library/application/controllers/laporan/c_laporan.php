<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_laporan extends CI_Controller {
	
	public function LaporanPengunjung()
	{
		if ($this->session->has_userdata('session_key')){
			$pengunjungPerBulan = $this->pengunjung->GetLaporanPengunjungPerBulan();
			$bulanForSelectOpt = $this->pengunjung->GetBulan();
			
			$data = array(
				'title'				=> 'Peminjaman',
				'aktif_trns'		=> 'active',
				'aktif_sub1'		=> 'active',
				'menu' 				=> 'Laporan',
				'sub1' 				=> 'Laporan Pengunjung',
				'bulan'				=> $bulanForSelectOpt,
				'pengunjungPerBulan'=> $pengunjungPerBulan
			);
			$this->load->view('layouts/masterHeader', $data);
			$this->load->view('layouts/masterNavbar');
			$this->load->view('laporan/v_laporanpengunjung');
			$this->load->view('layouts/masterFooter');
		}else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}
	
	public function LaporanPengunjungPerBulan()
	{
		$data = $this->pengunjung->GetLaporanPengunjungPerBulan();
		
		return $this->load->view('laporan/v_laporanpengunjungperbulan', array('data'=>$data));
	}
	
	public function LaporanPengunjungPerhari()
	{
		if($_POST['key'] != "")
		{
			$key = $_POST['key'];
			$data = $this->pengunjung->GetLaporanPengunjungPerHari($key);
			
			return $this->load->view('laporan/v_laporanpengunjungperhari', array('data'=>$data));
		}
		else
		{
			$this->LaporanPengunjungPerBulan();
		}
	}
	
	public function LaporanPeminjaman()
	{
		if ($this->session->has_userdata('session_key')){
			$peminjamanKeseluruhan = $this->peminjaman->GetLaporanPeminjamanKeseluruhan();
			$bulan = $this->peminjaman->GetBulan();
			
			$data = array(
				'title'					=> 'Peminjaman',
				'aktif_trns'			=> 'active',
				'aktif_sub1'			=> 'active',
				'menu' 					=> 'Laporan',
				'sub1' 					=> 'Laporan Peminjaman',
				'bulan'					=> $bulan,
				'peminjamanKeseluruhan'	=> $peminjamanKeseluruhan
			);
			$this->load->view('layouts/masterHeader', $data);
			$this->load->view('layouts/masterNavbar');
			$this->load->view('laporan/v_laporanpeminjaman');
			$this->load->view('layouts/masterFooter');
		}else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}
	public function LaporanPeminjamanKeseluruhan()
	{
		$data = $this->peminjaman->GetLaporanPeminjamanKeseluruhan();
		
		return $this->load->view('laporan/v_laporanpeminjamankeseluruhan', array('data'=>$data));
	}
	
	public function LaporanPeminjamanPerbulan()
	{
		if($_POST['key'] != "")
		{
			$key = $_POST['key'];
			$data = $this->peminjaman->GetLaporanPeminjamanPerbulan($key);
			
			return $this->load->view('laporan/v_laporanpeminjamanperbulan', array('data'=>$data));
		}
		else
		{
			$this->LaporanPeminjamanKeseluruhan();
		}
	}
}
