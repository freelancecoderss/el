<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_anggota extends CI_Controller {

	public function Form_Input_Show()
	{
		if ($this->session->has_userdata('session_key'))
		{
			$data = array
			(
				'title'		=>'Tambah Data Anggota',
				'aktif_ag'	=>'active',
				'aktif_menu'=>'active',
				'menu' 		=> 'Anggota',
				'sub1' 		=> 'Data Anggota',
				'sub2' 		=> 'Input Data Anggota',
				'id' => $this->anggota->AmbilIdTerakhir()
			);
			$this->load->view('layouts/masterHeader',$data);
			$this->load->view('layouts/masterNavbar');
			$this->load->view('anggota/v_halamaninputanggota');
			$this->load->view('layouts/masterFooter');
		}
		else
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}

	public function Form_Data_Show()
	{
		if ($this->session->has_userdata('session_key'))
		{
			$datah = array
			(
				'title'		=>'Lihat Data Anggota',
				'aktif_ag'	=>'active',
				'aktif_menu'=>'active',
				'menu' 		=> 'Anggota',
				'sub1' 		=> 'Data Anggota',

			);

			$hasil = $this->anggota->AmbilData();
			
			$data = array
			(
				'data' => $hasil,
				// 'jml' => $jml,
			);

			$this->load->view('layouts/masterHeader',$datah);
			$this->load->view('layouts/masterNavbar',$datah);
			$this->load->view('anggota/v_halamanlihatanggota', $data);
			$this->load->view('layouts/masterFooter');
		}
		else
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-info">Silahkan Melakukan Login Untuk Melanjutkan!</div>');
			redirect('c_halamanutama/loadFormLogin');
		}
	}

	public function Aksi_Tambah_Data()
	{
		$this->load->library('upload');
		
		$config['upload_path']          = './assets/images/anggota/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		
		$this->upload->initialize($config);
		
		$foto = substr($config['upload_path'],2).$_FILES["gambar"]["name"];
		
		if(!$this->upload->do_upload("gambar"))
		{
			$this->session->set_flashdata('pesan', $this->upload->display_errors());
			redirect(base_url('anggota/c_anggota/Form_Input_Show'));
		}
		else
		{
			$data = array('id_anggota' =>  $_POST['id'],
				'no_induk' => $_POST['induk'],
				'nama' => $_POST['nama'],
				'foto' => $foto
				);

			$hasil = $this->anggota->InsertData($data);

			if ($hasil) 
			{
				$this->session->set_flashdata('pesan', '<div class="alert alert-success">data berhasil diinsert</div>');
				redirect(base_url('anggota/c_anggota/Form_Input_Show'));
			}
			else
			{
				$this->session->set_flashdata('pesan', '<div class="alert alert-danger">data berhasil diinsert</div>');
				redirect(base_url('anggota/c_anggota/Form_Input_Show'));
			}	
		}
	}

	public function Form_Edit_Show($key)
	{
		$result = $this->anggota->GetWhere($key);
		$data = array(
			'id_anggota'=> $result[0] -> id_anggota,
			'no_induk' => $result[0] -> no_induk,
			'nama' => $result[0] -> nama,
			'foto' => $result[0]-> foto
			);

		$datab = array
		(
			'title'=>'Edit Data Anggota',
			'aktif_ag'=>'active',
			'aktif_menu'=>'active',
			'menu' 			=> 'Anggota',
			'sub1' 			=> 'Data Anggota',
			'sub2' 			=> 'Edit Data Anggota',

		);

		$this->load->view('layouts/masterHeader',$datab);
		$this->load->view('layouts/masterNavbar',$datab);
		//$this->load->view('admin/index');
		$this->load->view('anggota/v_halamaneditanggota', $data);
		$this->load->view('layouts/masterFooter',$datab);
	}

	public function Aksi_Edit_Data()
	{
		if($_FILES["gambar"]["name"] != "")
		{
			$this->load->library('upload');
			
			$config['upload_path']          = './assets/images/anggota/';
			$config['allowed_types']        = 'gif|jpg|jpeg|png';
			
			$this->upload->initialize($config);
			
			$foto = substr($config['upload_path'],2).$_FILES["gambar"]["name"];
			
			if(!$this->upload->do_upload("gambar"))
			{
				$this->session->set_flashdata('pesan', $this->upload->display_errors());
				redirect(base_url('anggota/c_anggota/Form_Data_Show'));
			}
			else
			{
				echo $_POST['id'];
				echo $_POST['induk'];
				echo $_POST['nama'];
				$data = array( 'id_anggota' => $_POST['id'],
					'no_induk' =>  $_POST['induk'],
					'nama' => $_POST['nama'],
					'foto' => $foto
					);

				$key = array('id_anggota' => $_POST['id']);
				$hasil = $this->anggota->EditData($data, $key);

				if ($hasil)
				{
					$this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil diubah</div>');
					redirect(base_url('anggota/c_anggota/Form_Data_Show'));
				}
				else
				{
					$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Data Gagal diubah</div>');
					redirect(base_url('anggota/c_anggota/Form_Data_Show'));
				}
			}
		}
		else
		{
			echo $_POST['id'];
			echo $_POST['induk'];
			echo $_POST['nama'];
			$data = array( 'id_anggota' => $_POST['id'],
				'no_induk' =>  $_POST['induk'],
				'nama' => $_POST['nama']
				);

			$key = array('id_anggota' => $_POST['id']);
			$hasil = $this->anggota->EditData($data, $key);

			if ($hasil)
			{
				$this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil diubah</div>');
				redirect(base_url('anggota/c_anggota/Form_Data_Show'));
			}
			else
			{
				$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Data Gagal diubah</div>');
				redirect(base_url('anggota/c_anggota/Form_Data_Show'));
			}
		}
	}

	public function Aksi_Hapus_data($key)
	{
		$data = array
		(
			'id_anggota' => $key,
		);
		
		$result = $this->anggota->HapusData($data);
		if ($result) 
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil dihapus</div>');
			redirect(base_url('anggota/c_anggota/Form_Data_Show'));
		}
		else
		{
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Data gagal dihapus</div>');	
			redirect(base_url('anggota/c_anggota/Form_Data_Show'));
		}
	}
}
