<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman extends CI_Model {
	
	public function AmbilHeaderData()
	{
			$data = $this->db->select('header_pinjam.id_pinjam,anggota.nama,tanggal_pinjam')
							->from('header_pinjam')
							->join('anggota', 'header_pinjam.id_anggota = anggota.id_anggota')
							->get();

			return $data->result();
	}
	public function getLastID()
	{
		$lstid = $this->db->select('id_pinjam')->from('header_pinjam')
						->order_by('id_pinjam', 'DESC')
						->limit(1)->get()->result();
		if (count($lstid) > 0) {
			$cekbln = substr($lstid[0]->id_pinjam, 3, 4);
			if ($cekbln != date('my')) {
				$data = 'PMJ'.date('my').'001';	
			}else{
				$lstno = substr($lstid[0]->id_pinjam, 7) + 1;
				$data = substr($lstid[0]->id_pinjam, 0, strlen($lstid[0]->id_pinjam) - strlen($lstno)).$lstno;
			}
		}else{
			$data = 'PMJ'.date('my').'001';
		}
		return $data;
	}
	public function getIdPinjam()
	{
		$data = $this->db->distinct()->select('header_pinjam.id_pinjam')
						->from('header_pinjam')
						->join('detail_pinjam', 'header_pinjam.id_pinjam = detail_pinjam.id_pinjam')
						->join('header_pengembalian', 'header_pengembalian.id_pinjam=header_pinjam.id_pinjam', 'left')
						->join('detail_pengembalian', 'detail_pengembalian.id_pengembalian=header_pengembalian.id_pengembalian AND detail_pinjam.id_buku=detail_pengembalian.id_buku', 'left')
						->where('detail_pengembalian.tanggal_dikembalikan IS NULL')
						->get()->result();
		return $data;
	}
	public function getDataPeminjam($id_pinjam='')
	{
		$id_pengembalian='';
		
		$cekIdPengembalian = $this->db->select('header_pengembalian.id_pengembalian')
									->from('header_pinjam')
									->join('header_pengembalian', 'header_pengembalian.id_pinjam=header_pinjam.id_pinjam')
									->where('header_pinjam.id_pinjam', $id_pinjam)->get()->result();
		
		if(!isset($cekIdPengembalian[0]->id_pengembalian))
		{
			$id_pengembalian = $this->pengembalian->getLastID();
		}
		else
		{
			$id_pengembalian = $cekIdPengembalian[0]->id_pengembalian;
		}
		
		
		$hasil = $this->db->select("anggota.nama, detail_pinjam.id_pinjam, detail_pinjam.id_buku, judul_buku,
								header_pinjam.tanggal_pinjam, header_pinjam.tanggal_kembali,
								IF(detail_pengembalian.tanggal_dikembalikan IS NOT NULL, 'Sudah Dikembalikan', 'Belum Dikembalikan') AS status,
								IF(detail_pengembalian.tanggal_dikembalikan IS NOT NULL, IF(detail_pengembalian.tanggal_dikembalikan > header_pinjam.tanggal_kembali, 'Terlambat', 'Tepat Waktu'), '-') AS keterangan")
						->from('detail_pinjam')
						->join('buku', 'detail_pinjam.id_buku=buku.id_buku')
						->join('header_pinjam', 'header_pinjam.id_pinjam=detail_pinjam.id_pinjam')
						->join('anggota', 'header_pinjam.id_anggota=anggota.id_anggota')
						->join('header_pengembalian', 'header_pinjam.id_pinjam=header_pengembalian.id_pinjam', 'left')
						->join('detail_pengembalian', 'header_pengembalian.id_pengembalian=detail_pengembalian.id_pengembalian AND
								detail_pinjam.id_buku=detail_pengembalian.id_buku','left')
						->where('detail_pinjam.id_pinjam',$id_pinjam)
						->where('detail_pengembalian.tanggal_dikembalikan IS NULL')
						->get();
		
		$data= array(
			'data'				=> $hasil->result(),
			'id_pengembalian'	=> $id_pengembalian
		);
		
		return $data;
	}
	public function getDetailPinjam($key='')
	{
		$data = $this->db->select("detail_pinjam.id_pinjam, detail_pinjam.id_buku, judul_buku,
								header_pinjam.tanggal_pinjam, header_pinjam.tanggal_kembali,
								IF(detail_pengembalian.tanggal_dikembalikan IS NOT NULL, 'Sudah Dikembalikan', 'Belum Dikembalikan') AS status,
								IF(detail_pengembalian.tanggal_dikembalikan IS NOT NULL, IF(detail_pengembalian.tanggal_dikembalikan > header_pinjam.tanggal_kembali, 'Terlambat', 'Tepat Waktu'), '-') AS keterangan")
						->from('detail_pinjam')
						->join('buku', 'detail_pinjam.id_buku=buku.id_buku')
						->join('header_pinjam', 'header_pinjam.id_pinjam=detail_pinjam.id_pinjam')
						->join('header_pengembalian', 'header_pinjam.id_pinjam=header_pengembalian.id_pinjam', 'left')
						->join('detail_pengembalian', 'header_pengembalian.id_pengembalian=detail_pengembalian.id_pengembalian AND
								detail_pinjam.id_buku=detail_pengembalian.id_buku','left')
						->where('detail_pinjam.id_pinjam',$key)
						->get();
		return $data->result();
	}
	public function SimpanHeaderPinjam($data)
	{
		$data = $this->db->insert('header_pinjam', $data);
		return $data;
	}

	public function SimpanDetailPinjam($data)
	{ 
		$data = $this->db->insert('detail_pinjam',$data);
		return $data;
	}
	public function fillPdfData($idAnggota,$idBuku)
	{
		$buku = [];
		$jBuku = [];
		$namaAnggota = $this->db->select('nama')->where('id_anggota', $idAnggota)->get('anggota')->result()[0]->nama;
		for ($i=0; $i < count($idBuku); $i++) { 
			$buku[] = $this->db->select('judul_buku')->where('id_buku',$idBuku[$i])->get('buku')->result();
		}
		foreach ($buku as $bk) {
			$jBuku[] = $bk[0]->judul_buku;
		}
		$data = array('nAnggota'=>$namaAnggota, 'jBuku'=>$jBuku);
		return $data;
	}
	public function GetLike($key)
	{
		$data = $this->db->distinct()->select('header_pinjam.id_pinjam')
						->from('header_pinjam')
						->join('detail_pinjam', 'header_pinjam.id_pinjam = detail_pinjam.id_pinjam')
						->join('header_pengembalian', 'header_pengembalian.id_pinjam=header_pinjam.id_pinjam', 'left')
						->join('detail_pengembalian', 'detail_pengembalian.id_pengembalian=header_pengembalian.id_pengembalian AND detail_pinjam.id_buku=detail_pengembalian.id_buku', 'left')
						->where('detail_pengembalian.tanggal_dikembalikan IS NULL')
						->get()->result();
		return $data;
	}
	
	public function GetBulan()
	{
		$data = $this->db->select('DISTINCT MONTH(tanggal_pinjam) AS idBulan, MONTHNAME(tanggal_pinjam) AS namaBulan, YEAR(tanggal_pinjam) AS tahun')
						 ->get('header_pinjam')->result();
		return $data;
	}
	
	public function GetPeminjamanHarusKembali()
	{
		$data = $this->db->query("SELECT header_pinjam.id_pinjam, anggota.nama, header_pinjam.tanggal_kembali
								 FROM header_pinjam
								 JOIN anggota USING (id_anggota)
								 JOIN detail_pinjam USING(id_pinjam)
								 LEFT JOIN header_pengembalian USING(id_pinjam)
								 LEFT JOIN detail_pengembalian USING(id_pengembalian, id_buku)WHERE header_pinjam.tanggal_kembali <= NOW()
								 AND detail_pengembalian.tanggal_dikembalikan IS NULL")->result();
		return $data;
	}
	
	public function getDetailHarusKembali($key)
	{
		$data = $this->db->query("SELECT buku.judul_buku, header_pinjam.tanggal_kembali
								 FROM header_pinjam
								 JOIN detail_pinjam USING(id_pinjam)
								 JOIN buku USING(id_buku)
								 LEFT JOIN header_pengembalian USING(id_pinjam)
								 LEFT JOIN detail_pengembalian USING(id_pengembalian, id_buku)
								 WHERE detail_pengembalian.tanggal_dikembalikan IS NULL
								 AND header_pinjam.id_pinjam='$key'")->result();
		return $data;
	}
	
	public function GetLaporanPeminjamanKeseluruhan()
	{
		$data = $this->db->query("SELECT anggota.id_anggota AS id_anggota, anggota.no_induk AS no_induk, anggota.nama AS nama,
								 COUNT(header_pinjam.id_pinjam) AS jumlah_pinjam, terlambat.terlambat_kembali, belum.belum_kembali
								 FROM header_pinjam
								 INNER JOIN anggota ON anggota.id_anggota=header_pinjam.id_anggota
								 LEFT JOIN 
								(SELECT anggota.id_anggota AS id_anggota,
								 COUNT(DISTINCT header_pengembalian.id_pengembalian) AS terlambat_kembali
								 FROM header_pengembalian
								 JOIN detail_pengembalian USING(id_pengembalian)
								 JOIN header_pinjam USING(id_pinjam)
								 JOIN anggota USING(id_anggota)
								 WHERE detail_pengembalian.tanggal_dikembalikan > header_pinjam.tanggal_kembali
								 GROUP BY header_pinjam.id_anggota) terlambat
								 ON terlambat.id_anggota=anggota.id_anggota
								 LEFT JOIN
								(SELECT header_pinjam.id_anggota, COUNT(detail_pinjam.id_buku) AS belum_kembali
								 FROM header_pinjam
								 JOIN detail_pinjam USING(id_pinjam)
								 LEFT JOIN header_pengembalian USING(id_pinjam)
								 LEFT JOIN detail_pengembalian USING(id_pengembalian, id_buku)
								 WHERE header_pinjam.tanggal_kembali <= NOW()
								 AND detail_pengembalian.tanggal_dikembalikan IS NULL) belum
								 ON belum.id_anggota=anggota.id_anggota
								 GROUP BY header_pinjam.id_anggota ORDER BY jumlah_pinjam DESC")->result();
		return $data;
	}
	
	public function GetLaporanPeminjamanPerbulan($bulan)
	{
		$data = $this->db->query("SELECT anggota.id_anggota AS id_anggota, anggota.no_induk AS no_induk, anggota.nama AS nama,
								 COUNT(header_pinjam.id_pinjam) AS jumlah_pinjam, terlambat.terlambat_kembali, belum.belum_kembali
								 FROM header_pinjam
								 INNER JOIN anggota ON anggota.id_anggota=header_pinjam.id_anggota
								 LEFT JOIN 
								(SELECT anggota.id_anggota AS id_anggota,
								 COUNT(DISTINCT header_pengembalian.id_pengembalian) AS terlambat_kembali
								 FROM header_pengembalian
								 JOIN detail_pengembalian USING(id_pengembalian)
								 JOIN header_pinjam USING(id_pinjam)
								 JOIN anggota USING(id_anggota)
								 WHERE MONTH(header_pinjam.tanggal_pinjam)=SUBSTRING('$bulan', 1, (LENGTH('$bulan')-5))
								 AND YEAR(header_pinjam.tanggal_pinjam)=SUBSTRING('$bulan',-4)
								 AND detail_pengembalian.tanggal_dikembalikan > header_pinjam.tanggal_kembali
								 GROUP BY header_pinjam.id_anggota) terlambat
								 ON terlambat.id_anggota=anggota.id_anggota
								 LEFT JOIN
								(SELECT header_pinjam.id_anggota, COUNT(detail_pinjam.id_buku) AS belum_kembali
								 FROM header_pinjam
								 JOIN detail_pinjam USING(id_pinjam)
								 LEFT JOIN header_pengembalian USING(id_pinjam)
								 LEFT JOIN detail_pengembalian USING(id_pengembalian, id_buku)
								 WHERE MONTH(header_pinjam.tanggal_pinjam)=SUBSTRING('$bulan', 1, (LENGTH('$bulan')-5))
								 AND YEAR(header_pinjam.tanggal_pinjam)=SUBSTRING('$bulan',-4)
								 AND header_pinjam.tanggal_kembali <= NOW()
								 AND detail_pengembalian.tanggal_dikembalikan IS NULL) belum
								 ON belum.id_anggota=anggota.id_anggota
								 WHERE MONTH(header_pinjam.tanggal_pinjam)=SUBSTRING('$bulan', 1, (LENGTH('$bulan')-5))
								 AND YEAR(header_pinjam.tanggal_pinjam)=SUBSTRING('$bulan',-4)
								 GROUP BY header_pinjam.id_anggota ORDER BY jumlah_pinjam DESC")->result();
		return $data;
	}
}
