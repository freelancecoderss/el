<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengunjung extends CI_Model {
	
	public function AmbilData()
	{
			$data = $this->db->query('SELECT * FROM pengunjung')->result();
			return $data;
	}
	public function HariIni()
	{

		$data = $this->db->like('waktu_berkunjung', date('Y-m-d'))
						->order_by('waktu_berkunjung', 'DESC')
						->limit(20)
						->get('pengunjung')->result();
		return $data;
	}
	public function AkumulasiPengunjung()
	{
		$data = $this->db->order_by('waktu_berkunjung DESC')
						->limit(15)
						->get('pengunjung')->result();
		return $data;
	}
	
	public function getLastID()
	{
		$lstid = $this->db->select('id_pengunjung')->from('pengunjung')
						->order_by('id_pengunjung', 'DESC')
						->limit(1)->get()->result();
		if (count($lstid) > 0) {
			$cekbln = substr($lstid[0]->id_pengunjung, 3, 4);
			if ($cekbln != date('my')) {
				$data = 'PNJ'.date('my').'001';	
			}else{
				$lstno = substr($lstid[0]->id_pengunjung, 7) + 1;
				$data = substr($lstid[0]->id_pengunjung, 0, strlen($lstid[0]->id_pengunjung) - strlen($lstno)).$lstno;
			}
		}else{
			$data = 'PNJ'.date('my').'001';
		}
		return $data;
	}
	
	public function InsertData($data)
	{
		$data=$this->db->insert('pengunjung', $data);
		return $data;
	}
	
	public function GetBulan()
	{
		$data = $this->db->select('DISTINCT MONTH(waktu_berkunjung) AS idBulan, MONTHNAME(waktu_berkunjung) AS namaBulan, YEAR(waktu_berkunjung) AS tahun')
						->get('pengunjung')->result();
		
		return $data;
	}
	
	public function GetLaporanPengunjungPerBulan()
	{
		$data = $this->db->query('SELECT CONCAT(MONTHNAME(waktu_berkunjung), " - " ,YEAR(waktu_berkunjung)) AS bulan,
								 COUNT(id_pengunjung) AS jumlah
								 FROM pengunjung
								 GROUP BY MONTH(waktu_berkunjung)
								 ORDER BY waktu_berkunjung ASC')->result();
		return $data;
	}
	
	public function GetLaporanPengunjungPerHari($bulan)
	{
		$data = $this->db->query("SELECT CONCAT(DAY(waktu_berkunjung), ' - ', MONTHNAME(waktu_berkunjung), ' - ', YEAR(waktu_berkunjung)) AS hari,
								 COUNT(id_pengunjung) AS jumlah
								 FROM pengunjung
								 WHERE MONTH(waktu_berkunjung)=SUBSTRING('$bulan', 1, (LENGTH('$bulan')-5))
								 AND YEAR(waktu_berkunjung)=SUBSTRING('$bulan',-4)
								 GROUP BY DAY(waktu_berkunjung)
								 ORDER BY waktu_berkunjung ASC")->result();
								 
		return $data;
	}
}
