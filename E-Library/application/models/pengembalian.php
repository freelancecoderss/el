<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian extends CI_Model {
	
	public function AmbilHeaderData()
	{
			$data = $this->db->select('header_pinjam.id_pinjam,anggota.nama,tanggal_pinjam,tanggal_kembali, buku.judul_buku')
							->distinct('header_pinjam.id_pinjam')
							->from('header_pinjam')
							->join('anggota', 'header_pinjam.id_anggota = anggota.id_anggota')
							->join('detail_pinjam', 'header_pinjam.id_pinjam = detail_pinjam.id_pinjam')
							->join('buku', 'detail_pinjam.id_buku = buku.id_buku')
							->get();

			return $data->result();
	}
	public function GetIdPengembalian($idPinjam)
	{
		$idPengembalian = $this->db->select('id_pengembalian')->from('header_pengembalian')
									->where('id_pinjam', $idPinjam)->get()->result();
		
		return $idPengembalian;
	}
	public function getLastID()
	{
		$lstid = $this->db->select('id_pengembalian')->from('header_pengembalian')
						->order_by('id_pengembalian', 'DESC')
						->limit(1)->get()->result();
		if (count($lstid) > 0) {
			$cekbln = substr($lstid[0]->id_pengembalian, 3, 4);
			if ($cekbln != date('my')) {
				$data = 'KMB'.date('my').'001';	
			}else{
				$lstno = substr($lstid[0]->id_pengembalian, 7) + 1;
				$data = substr($lstid[0]->id_pengembalian, 0, strlen($lstid[0]->id_pengembalian) - strlen($lstno)).$lstno;
			}
		}else{
			$data = 'KMB'.date('my').'001';
		}
		return $data;
	}
	public function SimpanHeaderKembali($data)
	{
		$data = $this->db->insert('header_pengembalian', $data);
		return $data;
	}

	public function SimpanDetailKembali($data)
	{
		$data = $this->db->insert('detail_pengembalian',$data);
		return $data;
	}
	
	public function GetLike($key)
	{
		$data = $this->db->query("SELECT * FROM header_pengembalian WHERE id_pengembalian LIKE '%$key%'")->result();
		return $data;
	}
}
