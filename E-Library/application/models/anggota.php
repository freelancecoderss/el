<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Model {

	public function AmbilData()
	{
		$data = $this->db->query('SELECT * FROM anggota')->result();
		return $data;
	}
	public function AnggotaBaru()
	{
		$data = $this->db->order_by('tgl_mendaftar', 'DESC')
						->limit(1)
						->get('anggota')->result();
		return $data;
	}
	
	public function AmbilIdTerakhir()
	{
		$data = $this->db->query('SELECT id_anggota FROM anggota ORDER BY id_anggota DESC LIMIT 0,1')->result();
		
		$idBaru;
		
		if(count($data) > 0)
		{
			$id = substr($data[0]->id_anggota, 0, 4);
			$noBaru = substr($data[0]->id_anggota, 4) + 1;
			$noLama = substr($data[0]->id_anggota, 4);
			
			$idBaru = $id.substr($data[0]->id_anggota, 4, (strlen($noLama) - strlen($noBaru))).$noBaru;
			return $idBaru;
		}
		else
		{
			$idBaru = date("Y")."0001";
			return $idBaru;
		}
	}
	
	public function InsertData($data)
	{
		$data = $this->db->Insert('anggota', $data);
		return $data;
	}

	public function GetWhere($key)
	{
		$data = $this->db->query("SELECT * FROM anggota WHERE id_anggota='$key'")->result();
		return $data;
	}

	public function GetLike($key)
	{
		$data = $this->db->query("SELECT * FROM anggota WHERE id_anggota LIKE '%$key%' OR nama LIKE '%$key%' OR no_induk LIKE '%$key%'")->result();
		return $data;
	}

	public function EditData($data, $key)
	{
		$data = $this->db->update('anggota', $data, $key);
		return $data;
	}

	public function HapusData($key)
	{
		$data = $this->db->delete('anggota', $key);
		return $data;
	}
}
