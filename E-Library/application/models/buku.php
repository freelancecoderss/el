<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Model {

	public function AmbilData()
	{
		$data = $this->db->query('SELECT * FROM buku')->result();
		return $data;
	}
	public function BukuBaru()
	{
		$data = $this->db->order_by('tgl_input', 'DESC')
						->limit(1)
						->get('buku')->result();
		return $data;
	}

	public function InsertData($data)
	{
		$data = $this->db->Insert('buku', $data);
		return $data;
	}

	public function GetWhere($key)
	{
		$data = $this->db->query("SELECT * FROM buku WHERE id_buku='$key'")->result();
		return $data;
	}

	public function GetLike($key)
	{
		$data = $this->db->query("SELECT * FROM buku WHERE id_buku LIKE '%$key%' OR judul_buku LIKE '%$key%' OR isbn LIKE '%$key%'")->result();
		return $data;
	}

	public function EditData($data, $key)
	{
		$data = $this->db->update('buku', $data, $key);
		return $data;
	}

	public function HapusData($key)
	{
		$data = $this->db->delete('buku', $key);
		return $data;
	}
}
