<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Model {

	public function VerfLogin($user,$pwd)
	{
		$data = $this->db->where('username', $user)->where('password',$pwd)->get('admin')->result();
		//$data = $this->db->query("SELECT * FROM admin WHERE username = '$user' AND password = '$pwd' ")->result();
		return $data;
	}
	
	public function AmbilData()
	{
		$data = $this->db->query('SELECT * FROM admin')->result();
		return $data;
	}
	
	public function InsertData($data)
	{
		$data=$this->db->insert('admin', $data);
		return $data;
	}
	
	public function GetWhere($where="")
	{
		$data = $this->db->query("SELECT * FROM admin WHERE id_admin='$where'")->result();
		return $data;
	}
	
	public function UpdateData($data, $key)
	{
		$data=$this->db->update('admin', $data, $key);
		return $data;
	}
	
	public function DeleteData($key)
	{
		$data=$this->db->delete('admin', $key);
		return $data;
	}
}
